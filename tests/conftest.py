import os

import pytest

from itemmaster import ItemMaster

ITEM_MASTER_LOGIN = None if 'ITEM_MASTER_LOGIN' not in os.environ else os.environ['ITEM_MASTER_LOGIN']


def pytest_addoption(parser):
    parser.addoption(
        "--user",
        action="store",
        default=None if ITEM_MASTER_LOGIN is None else ITEM_MASTER_LOGIN.split(':')[0],
        help="user: Item Master API user name"
    )
    parser.addoption(
        "--password",
        action="store",
        default=None if ITEM_MASTER_LOGIN is None else ITEM_MASTER_LOGIN.split(':')[-1],
        help="password: Item Master API password"
    )


@pytest.fixture
def im(request):
    credentials = (
        request.config.getoption("--user"),
        request.config.getoption("--password")
    )
    if None in credentials:
        raise EnvironmentError('\n'.join((
            'In order to perform unit tests for this module, please either set the following environment variables ' +
            'to reflect credentials for a current Item Master API account:',
            '    - "ITEM_MASTER_LOGIN" (in the format "user:password")',
            '...or run pytest with (both of) the following command line options:',
            '    --user',
            '    --password'
        )))
    user, password = credentials
    return ItemMaster(
        user=user,
        password=password
    )
