from itertools import chain
from random import shuffle

from datetime import datetime, date

from decimal import Decimal
from warnings import warn
from xml.etree.ElementTree import tostring, XML, ParseError

from collections import Iterable

from itemmaster import ItemMaster
from itemmaster.elements import Item, ValueWithID, Category, ManufacturerSuppliedContent, PackageData, Measurement, \
    PlanogramData, Product, Grocery, NutritionalClaims, ContainerRefund, FoodRelatedIndicators, Nutrition, UPC, \
    Nutrient, \
    Company, Attribute, SOAPElement
from itemmaster.elements import Medium
from itemmaster.elements import ServingSize
from itemmaster.elements import XFormSource

NoneType = type(None)


# Data Type Validations

def soap_element_test(se):
    # type: (SOAPElement) -> None
    assert isinstance(se, SOAPElement)
    for e, p_t in se.elements_properties.items():
        p, t = p_t
        v = getattr(se, p)
        if isinstance(t, (tuple, list)):
            t = t[0] if t else None
            if t is not None:
                vs = v
                if not isinstance(vs, Iterable):
                    raise TypeError(
                        'The value attributed to %s.%s is not iterable: %s' % (
                            type(se).__name__,
                            p,
                            repr(vs)
                        )
                    )
                for v in vs:
                    assert(isinstance(v, t))
                    if isinstance(v, SOAPElement):
                        soap_element_test(v)
        else:
            try:
                assert isinstance(v, (t, NoneType))
            except TypeError as ee:
                print(e + ' ' + p + ' ' + repr(t))
                raise ee
            if isinstance(v, SOAPElement):
                soap_element_test(v)


# Tests

def test_brands(
    im  # type: ItemMaster
):
    soap_element_test(im.brands())


def test_manufacturers(
    im  # type: ItemMaster
):
    soap_element_test(im.manufacturers())


def test_retailers(
    im  # type: ItemMaster
):
    soap_element_test(im.retailers())


def test_items(
    im  # type: ItemMaster
):
    n = 0
    brands = im.brands().brand
    shuffle(brands)
    for b in brands:
        print(b.name)
        for i in im.items(
            b=b.id,
            pi='o',
            epl=1000,
            epf=1000,
            epr=1000
        ).item:  # type: Item
            try:
                soap_element_test(i)
            except Exception as e:
                e.args = tuple(
                    chain(
                        (
                            'Errors occured while validating %s:\r\n\r\n%s\r\n\r\n%s' % (
                                ', '.join(u.text for u in i.upcs),
                                i._string or tostring(i._element, encoding='unicode'),
                                e.args[0] if e.args else ''
                            ),
                        ),
                        e.args[1:] if len(e.args) > 1 else tuple()
                    )
                )
                raise e
            assert len(i.media) > 0
        n += 1
        if n > 100:
            break

