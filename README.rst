itemmaster
==========

A python package for retrieving and parsing product data and images using `ItemMaster's API`_.

Requirements
------------

- You must have an API user name and password (this can be obtained by registering_ for a retailer account).
- Python 2.7 or 3.4+

To install:

.. code-block:: python

    pip install itemmaster

To follow and execute subsequent code examples, you will need to import the following:

.. code-block:: python

    from datetime import datetime, timedelta
    from itemmaster import ItemMaster

The following are only needed for `type hinting`_ in supporting IDE's:

.. code-block:: python

    from typing import Iterator
    from itemmaster.elements import Item, Brand, Manufacturer, Retailer

Authenticate & Connect
----------------------

To connect to ItemMaster's API, create an instance of the *ItemMaster* class:

.. code-block:: python

    item_master = ItemMaster(
        user='your_username',
        password='your_password'
    )

Items
-----

You can retrieve items from ItemMaster using the method *ItemMaster.items()*, which accepts the following parameters:

- **item_id** (str)
- **idx** (int): The index of the first item to return. This parameter is for use in conjunction with the *limit* to
  iteratively retrieve a limited subset of the items found matching your other parameters.
- **limit** (int): The number of items to return.
- **q** (*str*): Retrieve all items with this string in the description.
- **m** (*str*): Retrieve all items associated with this Manufacturer_ ID.
- **b** (*str*): Retrieve all items associated with this Brand_ ID.
- **upc** (*str*): Retrieve items associated with this UPC (GTIN).
- **since** (*date*): Retrieve items updated on or after this *date*.
- **until** (*date*): Retrieve items updated on or before this date.
- **pi** (*str*): Specifies which (if any) version of planogram images should be returned (defualts to *None*):

  + "o": Original
  + "c": Edited
  + "b": Both

- **ptga** (*int*): Specifies the file size for TGA-16 planogram images, in kilobytes. Defaults to 100KB. The options
  available are 50 (50KB) or 100 (100KB).
- **pip** (*int*): The resulting PPI of the planogram images. Specify only if you would like a different/lower PPI than
  the original image.
- **ef** (*str*): The resulting format of the ecommerce images. The options available are "jpg", "png", or "tif".
- **eip** (*int*): The resulting PPI of the ecommerce images.
- **epl** (*Tuple[int]* or *int*): The resulting size(s) of the ecommerce left image. All ItemMaster ecommerce images
  are square—so, for example—passing a value of *50* will cause the ecommerce image returned to be re-sized to 50 x 50
  pixels. This can be 50, 100, 150, 200, 600, 900, 1000, or a *tuple* of 2 or more of these values.
- **epf** (*Tuple[int]* or *int*): The resulting size(s) of the ecommerce front image. All ItemMaster ecommerce images
  are square—so, for example—passing a value of *50* will cause the ecommerce image returned to be re-sized to 50 x 50
  pixels. This can be 50, 100, 150, 200, 600, 900, 1000, or a *tuple* of 2 or more of these values.
- **epr** (*int*): The resulting size(s) of the ecommerce right image. All ItemMaster ecommerce images
  are square—so, for example—passing a value of *50* will cause the ecommerce image returned to be re-sized to 50 x 50
  pixels. This can be 50, 100, 150, 200, 600, 900, 1000, or a *tuple* of 2 or more of these values.

In the following example, we retrieve all the items which have been added/updated in the past week (not including
today), specifying that we want both planogram images *and* ecommerce images (in PNG format), and that we want our
ecommerce images to be 1000 x 1000 pixels.

.. code-block:: python

    for item in item_master.items(
        idx=0,
        limit=10,
        since=datetime.today() - timedelta(days=7),
        until=datetime.today() - timedelta(days=1),
        pi='b',
        ef='png',
        epl=1000,
        epf=1000,
        epr=1000
    ):  # type: Item
        print(item)

Interpreting Item Data
----------------------

The following table is an overview of elements and properties contained in an `Item`_ instance:

=============================== ========================================================================================
Class                           **Properties** (*types*)
=============================== ========================================================================================
_`Item`                         - **id** (*str*): A unique identifier for this item.
                                - **type** (*str*): A high-level classification for this item, such as "grocery".
                                - **status** (*str*): "P" indicates "published".
                                - **company_tracking_id** (`CompanyTrackingID`_)
                                - **item_master_name** (*str*): A one-line description of the item, typically identical
                                  to the *name*, as well as to the product description associated with the product with
                                  a sequence value of *1*.
                                - **sell_copy** (*str*): A brief paragraph containing marketing copy.
                                - **bullet_points** (*Sequence[str]*): A sequence of features, usually intended to
                                  accompany the *sell_copy*.
                                - **ecommerce_description** (*str*): A one-line description intended for ecommerce use.
                                - **manufacturer_ecommerce_description** (*str*): A one-line description intended for
                                  ecommerce use, provided by the manufacturer.
                                - **name** (*str*): A one-line description of the item.
                                - **marketing_description** (*str*): An often incoherent assemblage of text captured
                                  from the product's packaging—not typically usable for marketing purposes. Refer to
                                  *sell_copy*, when present, for more coherent text.
                                - **other_description** (*str*): Additional text captured from the product's packaging.
                                - **upcs** (Sequence[`UPC`_]): A sequence of one or more universal product codes used to
                                  identify this item.
                                - **manufacturer** (`ValueWithID`_): The name and unique ID of this product's
                                  manufacturer.
                                - **item_retailer** (`ValueWithID`_): The name and unique ID of this product's retailer.
                                - **categories** (Sequence[`Category`_]): A list of
                                  `GPC bricks <http://www.gs1.org/gpc>`_ associated with this item.
                                - **private_label_item** (*bool*): Is this a private label product?
                                - **created** (`XFormSource`_): When was this product created?
                                - **last_updated** (`XFormSource`_): When was this product last updated?
                                - **distributor** (`ValueWithID`_): The company responsible for distributing this
                                  product.
                                - **importer** (`ValueWithID`_): The company responsible for importing this product.
                                - **manufacturer_sku** (*str*): The manufacturer's stock keeping unit for this item.
                                - **media** (Sequence[`Medium`_]): Media (most commonly images) associated with the
                                  item.
                                - **manufacturer_supplied_content_images** (Sequence[`ManufacturerSuppliedContent`_]):
                                  Images, provided by the manufacturer, for this item.
                                - **manufacturer_supplied_content_logos** (Sequence[`ManufacturerSuppliedContent`_]):
                                  Logos, provided by the manufacturer, for this item.
                                - **manufacturer_supplied_content_videos** (Sequence[`ManufacturerSuppliedContent`_]):
                                  Videos, provided by the manufacturer, for this item.
                                - **manufacturer_supplied_content_files** (Sequence[`ManufacturerSuppliedContent`_]):
                                  Media files, provided by the manufacturer, for this item.
                                - **manufacturer_supplied_content_mobiles** (Sequence[`ManufacturerSuppliedContent`_]):
                                  Mobile optimized images, provided by the manufacturer, for this item.
                                - **company_supplied_images** (Sequence[`ManufacturerSuppliedContent`_]):
                                  Images, provided by the manufacturer, for this item.
                                - **package_data** (`PackageData`_): Information about the package, such as dimensions,
                                  package type, etc.
                                - **planogram_data** (`PlanogramData`_): Planogram and shelf management information.
                                - **products** (Sequence[`Product`_]): A sequence of `Product`_ instances, each
                                  containing information about the product which may vary for individual units contained
                                  within the consumer unit. When multiple instances of `Product`_ are associated with an
                                  item, it is typically because the item is a variety pack (sold as a single consumer
                                  unit), and different information applicable to items within the variety pack. The
                                  first instance of `Product`_, where the sequence property is *1*, typically applies to
                                  the product as a whole (and will, as a result, often exclude nutrition information and
                                  other more specific details).
                                - **package_distributor** (`Company`_): Information about the company responsible for
                                  distributing this item.
                                - **package_importer** (`Company`_): Information about the company responsible for
                                  importing this item.
                                - **package_manufacturer** (`Company`_): Information about the company responsible for
                                  manufacturing this item.
                                - **attributes** (Sequence[`Attribute`_]): A sequence of arbitrary attribute/value
                                  pairs.
------------------------------- ----------------------------------------------------------------------------------------
_`UPC`                          - **type** (*str*): "UPC" or "PLU".
                                - **text** (*str*): A universal product code.
------------------------------- ----------------------------------------------------------------------------------------
_`CompanyTrackingID`            - **company_id** (*str*): A unique identifier for this company.
                                - **company_name** (*str*): The company's name.
------------------------------- ----------------------------------------------------------------------------------------
_`ValueWithID`                  - **id** (*str*): A unique identifier.
                                - **text** (*str*): A text value corresponding to the *id*.
------------------------------- ----------------------------------------------------------------------------------------
_`Category`                     - **type** (*str*): Currently, the only taxonomy is "GS1".
                                - **id** (*str*): The category's unique ID within the taxonomy specified in the *type*
                                  property.
                                - **name** (*str*): The name of the category.
------------------------------- ----------------------------------------------------------------------------------------
_`XFormSource`                  - **id** (*str*): Identifies the creator of a record.
                                - **datetime** (*datetime*): The date/time at which a record was created.
------------------------------- ----------------------------------------------------------------------------------------
_`Medium`                       - **description** (*str*): A description of the referenced media.
                                - **url** (*str*): A URL where this media can be retrieved.
                                - **type** (*str*): "edited" or "unedited"
                                - **view** (*str*): A `GDSN product view code <http://
                                  www.gs1.org/sites/default/files/docs/gdsn/Product_Image_Specification.pdf>`_.
                                - **mime_type** (*str*): `The type of media <https://en.wikipedia.org/wiki/
                                  Media_type>`_.
                                - **image_source** (*str*): An identifier representing the source of the image.
                                - **added** (*datetime*): The date/time when the medium was added.
------------------------------- ----------------------------------------------------------------------------------------
_`ManufacturerSuppliedContent`  - **description** (*str*): A description of the referenced media.
                                - **url** (*str*): A URL where this media can be retrieved.
                                - **file_type** (*str*): Example: "customMobile" or "customImage".
                                - **mime_type** (*str*): `The type of media <https://en.wikipedia.org/wiki/
                                  Media_type>`_.
                                - **added** (*datetime*): When was this asset received from the supplier?
                                - **copy** (*str*): The text content of this image.
------------------------------- ----------------------------------------------------------------------------------------
_`PackageData`                  - **type** (*str*): Type of unit (such as "Item" or "Case").
                                - **approximation** (*bool*): Are these measurements approximated?
                                - **length** (`Measurement`_): Package length/depth—see the `GS1 package measurement
                                  rules <http://www.gs1.org/docs/gdsn/3.1/GDSN_Package_Measurement_Rules.pdf>`_.
                                - **height** (`Measurement`_): Package height—see the `GS1 package measurement rules
                                  <http://www.gs1.org/docs/gdsn/3.1/GDSN_Package_Measurement_Rules.pdf>`_.
                                - **width** (`Measurement`_): Package width—see the `GS1 package measurement rules
                                  <http://www.gs1.org/docs/gdsn/3.1/GDSN_Package_Measurement_Rules.pdf>`_.
                                - **weight** (`Measurement`_): The gross weight of the product, including the packaging.
                                - **units_in_package** (*int*): The number of units in each package.
                                - **package_type** (`ValueWithID`_): The type of packaging unit. Valid values include:
                                  "PEN", "OVERWRAP", "STICK", "POLYBAG", "BLISTER CARD", "CASE PACK", "ROLL",
                                  "BLISTER PACK", "BLISTER SUSTAINABLE PACK", "PLASTIC CLAMSHELL", "UNIT",
                                  "RESEALABLE BAG", "SHRINKWRAP", "LOAF", "PUMP", "TRAY", "TUBE", "POT", "TUB",
                                  "TIN", "PACKET", "QT", "PINT", "PAN", "KEG", "LOG", "JUG", "JAR", "GALLON", "CUP",
                                  "BOWL", "NONE", "CARTON", "CAN", "BOTTLE", "BOX", "BIN", "BAR", "BAG", "STAND PACK",
                                  "CANISTER", "SLEEVE", "PLASTIC CONTAINER", "POUCH", "SPRAY CAN", "PACKAGE",
                                  "CONTAINER", "PACK", and "CARD".
                                - **package_size** (`Measurement`_): The size of the product, as described on the
                                  package.
                                - **net_weight** (`Measurement`_): Actual, computed, or estimated weight of the item
                                  without its container and/or packaging.
                                - **dry_weight** (`Measurement`_): The net weight of the product, when empty.
                                - **information** (*str*)
                                - **recyclable** (*bool*): Is this item recycleable?
------------------------------- ----------------------------------------------------------------------------------------
_`PlanogramData`                - **shelf_man_flag** (*bool*): Shelf management
                                - **depth_count** (*Decimal*)
                                - **width_count** (*Decimal*)
                                - **height_count** (*Decimal*)
                                - **depth_nesting** (*Decimal*)
                                - **vertical_nesting** (*Decimal*)
                                - **display_depth** (*Decimal*)
                                - **display_width** (*Decimal*)
                                - **division_name** (*str*)
                                - **dual_nesting** (*str*)
                                - **multiple_shelf_facings** (*int*)
                                - **peg_down** (*Decimal*)
                                - **peg_right** (*Decimal*)
                                - **tray_count** (*Decimal*)
                                - **tray_depth** (*Decimal*)
                                - **tray_width** (*Decimal*)
                                - **tray_height** (*Decimal*)
------------------------------- ----------------------------------------------------------------------------------------
_`Product`                      - **sequence** (*int*): If this is equal to *1*, the information applies to all products
                                  contained in the item/package.
                                - **id** (*str*): A unique identifier for this product.
                                - **type** (*str*): A high-level product classification, such as "grocery".
                                - **size** (`Measurement`_)
                                - **description** (*str*): A one-line description of the product, usually beginning with
                                  the brand name, for example: "Parks Famous Flavor Hot Smoked Turkey Sausage" or
                                  "Hatfield Recipe Essentials Ground Sausage Sweet Italian".
                                - **distributor** (`ValueWithID`_): The name and a unique identifier of the company who
                                  distributes this product.
                                - **importer** (`ValueWithID`_): Identifies the company responsible for importing this
                                  product.
                                - **brand** (`ValueWithID`_): Identifies the brand name of this product.
                                - **sub_brands** (*Sequence[str]*): Secondary levels of branding attributed to the
                                  product, often a trademarked name for a product line. In "Coca-Cola Classic",
                                  "Coca-Cola" is the brand and "Classic" is the sub-brand.
                                - **brand_description** (*str*): This is usually the same as *brand.text*, but not tied
                                  to a brand ID, and therefore can vary by item, and will occasionally include
                                  sub-branding such as "Coca-Cola Classic" rather than simply "Coca-Cola".
                                - **secondary_brand_description** (*str*): A secondary free-form description of the
                                  brand.
                                - **manufacturer** (`ValueWithID`_): This identifies the manufacturer of the product.
                                - **line** (*str*): The name of the the product line to which this belongs. A *product
                                  line* is a group of related products, under a single brand, sold by the same company.
                                - **variety** (*str*): A differentiation between variations of a *product line* such as
                                  a flavor, color, scent, etc.
                                - **seasonal** (*bool*): If *True*, this indicates that the item is only available for a
                                  limited period of distribution each year.
                                - **country_of_origin** (*str*): A comma-separated list of the country or countries
                                  where this product was grown, manufactured, or assembled.
                                - **warnings** (*str*): Warnings and precautions (usually those which are found on a
                                  product's
                                  packaging).
                                - **drug_interactions** (*str*): Information labeled "interactions" on over-the-counter
                                  or prescription medications.
                                - **directions** (*str*): Usage instructions for this product, as found on the product's
                                  packaging.
                                - **indications** (*str*): Indicates what ailments a product (typically an
                                  over-the-counter medication) can/should be used to treat.
                                - **grocery** (`Grocery`_): A collection of attributes exclusively pertinent to grocery
                                  items.
------------------------------- ----------------------------------------------------------------------------------------
 _`Grocery`                     - **ndc_code** (*str*): The "National Drug Code" is a unique, 10-digit, 3-segment
                                  number. It is a universal product identifier for human drugs in the United States.
                                  This code is required to be present on all over-the-counter and prescription
                                  medication packages and inserts in the United States.
                                - **npn_code** (*str*): The "Natural Product Number" is an 8-digit number issued for
                                  products which have been evaluated and licensed for sale in Canada by
                                  `Health Canada`_.
                                - **din_code** (*str*): The Drug Identification Number (DIN) is the 8 digit number
                                  located on the label of prescription and over-the-counter drug products that have been
                                  evaluated by the Therapeutic Products Directorate (TPD) and approved for sale in
                                  Canada.
                                - **alcohol_by_volume_percent** (*str*): A description of the percentage of alcohol in
                                  this beverage, potentially including descriptive modifiers composed of non-numeric
                                  digits such as "less than", "approximately", etc.
                                - **alcohol_type** (*str*)
                                - **ingredients** (*str*): A list of ingredients in this product, as described on the
                                  product packaging. For example: "Milk Chocolate (Sugar, Chocolate, Cocoa Butter, Skim
                                  Milk, Lactose, Milkfat, Soy Lecithin, Artificial and Natural Flavors, Salt), Sugar,
                                  Corn Syrup, Hydrogenated Palm Kernel Oil, Milkfat, Skim Milk, Cornstarch,
                                  Less Than 1% - Lactose, Dextrin, Salt, Mono and Diglycerides, Coloring (Includes Blue
                                  1 Lake, Yellow 6, Red 40, Yellow 5, Blue 1), Chocolate, Artificial Flavor, Gum
                                  Acacia.".
                                - **vitamins_and_minerals** (*str*): A list of vitamins and minerals contained in this
                                  product, as described on the packaging. For example: "Added Sugars-6g-12%, Vitamin
                                  D-0mcg-0%, Calcium-0mg-0%, Iron-0.84mg-4%.".
                                - **nutritional_claims** (`NutritionalClaims`_): This identifies nutrition claims
                                  asserted on a product's packaging.
                                - **container_refunds** (Sequence[`ContainerRefund`_]): A series of `ContainerRefund`_
                                  instances which indicate the deposit amount for this item's container, by state.
                                - **recipes** (*Sequence[str]*): A list of recipes found on the packaging of this
                                  product.
                                - **food_related_indicators** (`FoodRelatedIndicators`_): A collection of indicators
                                  which are pertinent to food products, including compliance with various dietary
                                  restrictions, preparation state, storage temperature, etc.
                                - **nutritions** (Sequence[`Nutrition`_]): A sequence of `Nutrition`_ instances, each
                                  representing a nutrition panel from the product packaging.
                                - **certifications** (*Sequence[str]*): A list of certifications applicable to this
                                  product.

                                  + 130: USDA Organic
                                  + 131: Non-GMO Project Certified
                                  + 132: Certified Gluten Free
                                  + 133: Certified B Corporation
                                  + 134: Quality Assurance International Certified Organic
                                  + 135: Certified Vegan
                                  + 136: California Certified Organic Farmers
                                  + 137: Fair Trade Certified
                                  + 138: Rainforest Alliance Certified
                                  + 139: Oregon Tilth Certified Organic
                                  + 140: American Heart Association Heart Healthy Certification
                                  + 141: American Humane Association
                                  + 142: Animal Welfare Approved
                                  + 143: Aurora Certified Organic
                                  + 144: Best Aquaculture Practices Certification
                                  + 145: Bird Friendly Coffee (Smithsonian Migratory Bird Center)
                                  + 146: Blue Angel Certification
                                  + 147: British Retail Consortium
                                  + 148: CE Marking
                                  + 149: Certified Angus Beef
                                  + 150: Certified Humane
                                  + 151: Cradle-to-Cradle
                                  + 152: Dolphin Safe
                                  + 153: US EPA Safer Choice
                                  + 154: Fish Wise
                                  + 155: Food Justice
                                  + 156: Forest Stewardship Council
                                  + 157: Food Safety System Certification 22000
                                  + 158: Good Housekeeping
                                  + 159: Good Housekeeping Green Seal
                                  + 161: Green Shield Certified
                                  + 162: Halal(Ifranca)
                                  + 163: Intertek
                                  + 164: Leaping Bunny (Coalition for consumer Information on Cosmetics)
                                  + 165: Marine Stewardship Council
                                  + 166: NSF Certification
                                  + 167: REAL® Seal
                                  + 168: Roundtable on Sustainable Palm Oil
                                  + 169: Safe Quality Food
                                  + 170: Salmon Safe
                                  + 171: Underwriters Laboratory
                                  + 172: USDA Certified Biobased Product
                                  + 173: USDA Process Verified Grassfed
                                  + 174: Whole Grains Council Stamps
                                  + 175: Nature's Healthiest Certified

                                - **kosher_codes** (*Sequence[str]*): A list of `kosher certifications`_ applicable to
                                  this product. Valid values include:

                                  + 1: The Union of Orthodox Jewish Congregation
                                  + 2: The Organized Kashruth Laboratories
                                  + 3: Chicago Rabbinical Council
                                  + 4: Star-D Kosher Supervision of the National Council of the Young Israel
                                  + 5: Kosher Supervision of America
                                  + 6: KOF-K Kosher Supervision
                                  + 7: Star K Kosher Certification
                                  + 8: K'hal Adath Jeshuren, NY, NY
                                  + 9: Shofar Kosher Foods
                                  + 10: Rabbinical Council of New England, (KVH)
                                  + 11: Rabbi M. Small Chicago
                                  + 12: The Diamond K
                                  + 13: Dairy
                                  + 14: Triangle Kosher Certification
                                  + 15: Kosher all year including Passover
                                  + 16: Kosher Overseers
                                  + 17: Unknown Circle U Organization
                                  + 18: Scroll K/Vaad Hakashrus of Denver
                                  + 19: United Mehadrin Kosher
                                  + 20: Kashrut Division of the London Beth Din, Court of the Chief Rabbi
                                  + 21: Unknown Kosher Dairy Organization
                                  + 22: Capital District Vaad Hakashruth
                                  + 23: Vaad Hakashrus of Buffalo
                                  + 24: Unknown Kosher Certification
                                  + 25: Orthodox Jewish Council Kosher Technical Konsultants
                                  + 26: Rabbi Yechiel Babad (Tartikover Rav),
                                  + 27: Dallas Kosher (Vaad Hakashrus of Dallas
                                  + 28: Unknown Kosher Certification
                                  + 29: United Mehadrin Kosher
                                  + 30: Orthodox Rabbinical Council of South Florida
                                  + 31: Ko Kosher Service
                                  + 32: K'hal Adath Jeshurun
                                  + 33: Orthodox Rabbicinical Council of British Columbia
                                  + 34: Kosher Supervisors of Wisconsin
                                  + 35: Metropolitan Kashruth Council of Michigan
                                  + 36: Rabbinical Council of New England - Rabbi Abraham Halbfinger - Boston, MA
                                  + 37: Natural Food Certifiers
                                  + 38: California K Igud Hakashrus of Los Angeles (Kehillah Kosher)
                                  + 39: THE HEART "K" (Kehillah Kosher)
                                  + 40: Certified Kosher Underwriters
                                  + 41: Central Rabbinical Congress (Hisachdus Harabanim
                                  + 42: Blue Ribbon Kosher
                                  + 43: Atlanta Kashrus Commission
                                  + 44: Debracin (Rabbi Shlomo Stern)
                                  + 45: Florida K and Florida Kashrus Services
                                  + 46: Jersey Shore Orthodox Rabbinate (J.S.O.R.)
                                  + 47: Rabbi Benjamin Kaplinsky
                                  + 48: Kashrus Council of Lakewood N.J.
                                  + 49: Council of Orthodox Rabbis of Greater Detroit (K-COR)
                                  + 50: Young Israel of West Hempstead
                                  + 51: Kosher Certification Service
                                  + 52: The Lehigh Valley Kashrus Commission (LVKC)
                                  + 53: Rabbi Yitzchok M. Leizerowski
                                  + 54: National Kashrus (NK)
                                  + 55: Rabbinical Council of Orange County &amp; Long Beach (Orange K)
                                  + 56: South Palm Beach Vaad (ORB)
                                  + 57: Org. of Orthodox Kashrus Supervision
                                  + 58: International Kosher Supervision/Texas "K" Chicago Rabbinical Council
                                  + 59: Orthodox Vaad of Philadelphia
                                  + 60: Rabbinical Council of California (RCC)
                                  + 61: Star-K Kosher Certification (chalav Yisrael) &amp; Star-D Certification
                                    (non-chalav Yisrael)
                                  + 62: Rabbi Nuchem Efraim Teitelbaum (Volover Rav)
                                  + 63: Vaad Hakashrus of K'hal Machzikei Hadas of Belz
                                  + 64: Dallas Kosher (Vaad Hakashrus of Dallas)
                                  + 65: Vaad Hakashrus of Northern California Administrative Offices
                                  + 66: Vaad Hakashrus of Rochester (VKR)
                                  + 67: Vaad Harabanim of the Five Towns and Far Rockaway
                                  + 68: Vaad Harabanim of Flatbush
                                  + 69: Vaad Harabanim of Greater Seattle
                                  + 70: Vaad Harabanim of Greater Washington
                                  + 71: Vaad of Lancaster / Cong. Degel Israel
                                  + 72: Kedassia, The Joint Kashrus Committee of England|
                                  + 73: K'hal Machzikei Hadas Edgeware
                                  + 74: London Beth Din Kashrut Division
                                  + 75: Machzikei Hadas Manchester
                                  + 76: Manchester Beis Din
                                  + 77: Dayan Osher Yaakov Westheim
                                  + 78: Kashrus Council of Canada (COR)
                                  + 79: Montreal Vaad Hair
                                  + 80: The NSW Kashrus Authority|
                                  + 81: The Beis Din Tzedek of Agudas Israel Moetzes Hakashrus
                                  + 82: The Beis Din Tzedek of the Eidah Hachareidis of Jerusalem Binyanei Zupnick
                                  + 83: The Beis Din Tzedek of K'hal Machzikei Hadas - Maareches Hakashrus
                                  + 84: Chug Chasam Sofer
                                  + 85: Rabbi Moshe Yehudah Leib Landau
                                  + 86: Rabanut Hareishit Rechovot
                                  + 87: Rabanut Yerushalayim Mehadrin
                                  + 88: Shearis Yisrael
                                  + 89: S.I.K.S. Ltd./ Services International Kosher Supervision
                                  + 90: Communidade Ortodoxa Israelita Kehillas Hachareidim Departmento de Kashrus
                                  + 91: HKK Kosher Certification Service
                                  + 92: Adas Yereim of Paris Rabbi Y.D. Frankfurter
                                  + 93: Rabbi Mordechai Rottenberg (Chief Orthodox Rav of Paris)
                                  + 94: Kashrut Department of Maguen David Community in Mexico City
                                  + 95: K'hal Chizuk Hadas of Flatbush
                                  + 96: Beis Din of Crown Heights Vaad Hakashrus
                                  + 97: Orthodox Rabbinical Council of British Columbia
                                  + 98: Vaad Hoeir of Saint Louis
                                  + 99: Rabbi Gershon Mendel Garelik - Italy
                                  + 100: Chug Chasam Sofer
                                  + 101: The Bais Din Tzedek of Agudath Israel, Moetzes HaKashrus
                                  + 102: Kedassia - The Joint Kashrus Committee of England
                                  + 103: Bais Ben Zion Kosher Certification
                                  + 104: Rabbi Shmuel Yaffa-Shlessinger (Strasbourg)
                                  + 105: Kashrus Department of the Beth Din of Johannesburg
                                  + 106: The Association for Reliable Kashrus
                                  + 107: Igud Hakashrus of Los Angeles(Kehillah Kosher)
                                  + 108: Vaad Hakashrus of Northern California
                                  + 109: Kosher Miami Vaad HaKashrus of Miami-Dade
                                  + 110: Orthodox Rabbinate of North Dade
                                  + 111: Indianapolis Beth Din
                                  + 112: Indianapolis Orthodox Board of Kashrus
                                  + 113: Iowa "Chai-K" Kosher Supervision
                                  + 114: Louisville Vaad Hakashrut
                                  + 115: Louisiana Kashrut Committee
                                  + 116: New England Kashrus LeMehadrin
                                  + 117: Vaad Hakashrus of Worcester
                                  + 118: Double U Kashrus Badatz Mehadrin USA
                                  + 119: Rabbinical Council of Bergen County
                                  + 120: Central Rabbinical Congress (Hisachdus HaRabanim)
                                  + 121: Vaad HaRabonim of Queens
                                  + 122: Vaad Harabanim of the Five Towns and Far Rockaway
                                  + 123: Cleveland Kosher
                                  + 124: Achdus Yisroel
                                  + 125: Melbourne Kashrut
                                  + 126: Far East Kashrut
                                  + 127: Gateshead Kashrus Authority
                                  + 128: Earth K
                                  + 129: Kosher Information Bureau
                                  + 130: Central California Kosher
                                  + 131: IKS Chicago Rabbinical Council
                                  + 132: Oregon Kosher
                                  + 133: Houston Kashruth Association
                                  + 134: Global Kosher
                                  + 135: Rabbi Mordechai Kaplinsky
                                  + 136: Quality Kosher Supervision
                                  + 137: Vaad Hakashrus of Tidewater, VA
                                  + 138: Vaad of Cincinnati
                                  + 139: Community Kashrut of Greater Philadelphia
                                  + 140: Kosher Los Angeles (KOLA)
                                  + 141: Va'ad HaKashruth of the Capital District (Albany, NY)
                                  + 142: Rabbi Aaron Teitelbaum (Nirbater Rav)
                                  + 143: New Square Kosher Council
                                  + 144: Rabbi Binyamin Gruber
                                  + 145: Rabbi M. Weissmandl (Rav of Nitra-Monsey)
                                  + 146: Kosher Maguen David Mexico
                                  + 147: Bedatz Mehadrin, Rehovot
                                  + 148: Kosher Kiwi of New Zealand
                                  + 149: Vaad Hakashrus of Northern California
                                  + 150: Midwest Kosher
                                  + 151: Kosher Organics
                                  + 152: Star-K Kosher Certification (chalav Yisrael)
                                  + 153: Coordinated Kosher Supervision
                                  + 154: Rabbi Aryeh Geretz
                                  + 155: SKS Kosher Certification Services
                                  + 156: Vaad Hakehilot of Memphis
                                  + 157: Federation of Synagogues
                                  + 158: Ajdut Israel Kosher
                                  + 159: UK Kashrut
                                  + 160: PARVE
                                  + 161: The "United States K"
                                  + 162: PAREVE
                                  + 163: Triangle Kosher Certification
                                  + 164: Vaad Ha'ir of Winnipeg
                                  + 165: Kosher Check
                                  + 166: Kosher Supervisores En Alimentos S.C.
                                  + 167: Cherry K Vaad Hakashruth
                                  + 168: Harav A. Wosner, Chug Chatam Sofer, Petach Tikvah, Israel
                                  + 169: Kosher Michigan Certification Agency
                                  + 170: Kosher Orthodox Certification Service
                                  + 171: Lancaster County Kosher
                                  + 172: Organization of Orthodox Kashruth Supervision
                                  + 173: Ottawa Vaad Hakashrut(OVH)
                                  + 174: Rabbi Nahum Ephraim Teitelboim, Volover Rav, NY, U.S.A.
                                  + 175: Rabino Abraham Benhamu, Communidad Judia del Peru
                                  + 176: Kosher Parve Madrid
                                  + 999: Generic Kosher code "K"
------------------------------- ----------------------------------------------------------------------------------------
_`Company`                      - **name** (*str*)
                                - **address** (*str*)
                                - **address2** (*str*)
                                - **city** (*str*)
                                - **state** (*str*)
                                - **zip** (*str*)
                                - **country** (*str*)
                                - **email** (*str*)
                                - **phone** (*str*)
                                - **url** (*str*)
                                - **fax** (*str*)
------------------------------- ----------------------------------------------------------------------------------------
_`Attribute`                    - **name** (*str*)
                                - **value** (*str*)
------------------------------- ----------------------------------------------------------------------------------------
_`ContainerRefund`              - **local** (*str*): An abbreviation referencing the locale/state to which this
                                  deposit/refund applies.
                                - **amount** (*str*): The deposit amount.
------------------------------- ----------------------------------------------------------------------------------------
_`Measurement`                  - **measure** (*Decimal*): A numeric measurement.
                                - **uom** (*str*): The unit in which the *measure* is expressed.

                                  + "BOX": # of boxes
                                  + "CT": count (# of units)
                                  + "CUP": # of cups
                                  + "FL OZ": fluid ounce
                                  + "FT": foot
                                  + "G": gram
                                  + "GAL": gallon
                                  + "IN": inch
                                  + "KIT": # of kits
                                  + "KG": kilogram
                                  + "KT": # of kits
                                  + "L": liter
                                  + "LB": pound
                                  + "MG": milligram
                                  + "ML": milliliter
                                  + "OZ": ounce
                                  + "PACK": # of packs
                                  + "PACKAGE": # of packs
                                  + "PIECE(S)": # of peices in a set
                                  + "PR": pair
                                  + "PT": pint
                                  + "QT": quart
                                  + "SQ FT": square foot
                                  + "SQ IN": square inch
                                  + "TBS": tablespoon
                                  + "YARDS": yard
                                  + "BAR": This unit of measure is usually accompanied by an empty value and should be
                                    ignored
------------------------------- ----------------------------------------------------------------------------------------
_`FoodRelatedIndicators`        - **temperature_indicator** (*str*): "Frozen", "Chilled", "Perishable", or "Shelf
                                  Stable".
                                - **vegan** (*bool*)
                                - **vegetarian** (*bool*)
                                - **lactose_free** (*bool*)
                                - **flavor** (*bool*): Indicates whether the product contains artificial flavoring.
                                - **antibiotic_free** (*bool*)
                                - **wheat_free** (*bool*)
                                - **gluten_free** (*bool*)
                                - **hormone_free** (*bool*)
                                - **natural** (*bool*)
                                - **nitrates_free** (*bool*)
                                - **nitrites_free** (*bool*)
                                - **organic** (*bool*)
                                - **peanut_free** (*bool*)
                                - **ready_to_cook** (*bool*)
                                - **ready_to_heat** (*bool*)
                                - **dairy_free** (*bool*)
                                - **egg_free** (*bool*)
                                - **non_gmo** (*bool*)
                                - **kosher** (*bool*)
                                - **shellfish_free** (*bool*)
                                - **soy_free** (*bool*)
                                - **milk_free** (*bool*)
                                - **crustacean_free** (*bool*)
                                - **fish_free** (*bool*)
                                - **sesame_free** (*bool*)
                                - **nut_free** (*bool*)
                                - **halal** (*bool*)
                                - **paleo** (*bool*)
------------------------------- ----------------------------------------------------------------------------------------
_`Nutrition`                    - **sequence** (*int*)
                                - **title** (*str*): Header text on this nutrition panel.
                                - **number_served_in_package** (*str*)
                                - **number_of_servings** (*str*)
                                - **serving_sizes** (Sequence[`ServingSize`_]): A list of `ServingSize`_ measurements,
                                  all referencing the same amount, but potentially quantified using differing
                                  units of measure. Typically, if more than one measurement is given, one
                                  will be metric, and the other will be imperial (US).
                                - **serving_size_full_txt** (*str*)
                                - **racc_amt** (`Measurement`_): A reference amount is a specific, regulated quantity of a
                                  type of food usually eaten  by an individual at one sitting, as established by
                                  `Health Canada`_.
                                - **energy** (*int*): The number of calories in a serving.
                                - **saturated_fat** (`Nutrient`_)
                                - **polyunsaturated_fat** (`Nutrient`_)
                                - **monounsaturated_fat** (`Nutrient`_)
                                - **trans_fat** (`Nutrient`_)
                                - **cholesterol** (`Nutrient`_)
                                - **sodium** (`Nutrient`_)
                                - **potassium** (`Nutrient`_)
                                - **carbohydrates** (`Nutrient`_)
                                - **dietary_fiber** (`Nutrient`_)
                                - **soluable_fiber** (`Nutrient`_)
                                - **insoluable_fiber** (`Nutrient`_)
                                - **sugars** (`Nutrient`_)
                                - **sugar_alchohol** (`Nutrient`_)
                                - **other_carbohydrates** (`Nutrient`_)
                                - **protein** (`Nutrient`_)

                                The following properties describe the percentage of each nutrient contained in a
                                serving, potentially including descriptive modifiers composed of non-numeric digits such
                                as "less than", "approximately", etc., in addition to numeric quantities.

                                - **daily_percent_of_vitamin_a** (*str*)
                                - **daily_percent_of_vitamin_c** (*str*)
                                - **daily_percent_of_calcium** (*str*)
                                - **daily_percent_of_iron** (*str*)
                                - **daily_percent_of_copper** (*str*)
                                - **daily_percent_of_folic_acid** (*str*)
                                - **daily_percent_of_iodine** (*str*)
                                - **daily_percent_of_magnesium** (*str*)
                                - **daily_percent_of_niacin** (*str*)
                                - **daily_percent_of_phosphorous** (*str*)
                                - **daily_percent_of_riboflavin** (*str*)
                                - **daily_percent_of_thiamin** (*str*)
                                - **daily_percent_of_vitamin_b12** (*str*)
                                - **daily_percent_of_vitamin_b6** (*str*)
                                - **daily_percent_of_vitamin_d** (*str*)
                                - **daily_percent_of_vitamin_e** (*str*)
                                - **daily_percent_of_vitamin_zinc** (*str*)
------------------------------- ----------------------------------------------------------------------------------------
_`Nutrient`                     - **text** (*str*): A numeric measurement (expressed as text).
                                - **uom** (*str*):

                                  + "kg": kilogram
                                  + "g": gram
                                  + "mg": milligram
                                  + "mcg": Microgram
                                  + "IU": international units
                                  + "PCT": percentage

                                - **daily_pct** (*str*): % of daily value—potentially including modifiers such as "<" or
                                  "~", or a footnote reference such as "*" or "**" in addition to a numeric quantity.
                                - **calories** (*int*): # of calories—potentially including modifiers such as "<" or
                                  "~", or a footnote reference such as "*" or "**" in addition to a numeric quantity.
------------------------------- ----------------------------------------------------------------------------------------
_`ServingSize`                  - **measure** (*Decimal*): A numeric measurement.
                                - **uom** (*str*): The unit of measure.
                                - **type** (*str*): "us" or "metric"
------------------------------- ----------------------------------------------------------------------------------------
_`NutritionalClaims`            - **fat_free** (*bool*)
                                - **good_source_of_fiber** (*bool*)
                                - **low_fat** (*bool*)
                                - **low_sodium** (*bool*)
                                - **sugar_free** (*bool*)
=============================== ========================================================================================


Brands, Manufacturers & Retailers
---------------------------------

.. _Brand:
.. _Manufacturer:
.. _Retailer:

A list of brands, manufacturers, or retailers (and their corresponding IDs) can be retrieved using the methods
*ItemMaster.brands()*, *ItemMaster.manufacturers()*, or *ItemMaster.retailers()*, respectively:

.. code-block:: python

    for brand in item_master.brands():  # type: Brand
        print('Brand ID: %s\nBrand Name: %s\n\n' % (brand.id, brand.name))

    for manufacturer in item_master.manufacturers():  # type: Manufacturer
        print('Manufacturer ID: %s\nManufacturer Name: %s\n\n' % (manufacturer.id, manufacturer.name))

    for retailer in item_master.retailers():  # type: Retailer
        print('Retailer ID: %s\nRetailer Name: %s\n\n' % (retailer.id, retailer.name))

..  _`ItemMaster's API`: https://api.itemmaster.com/v2.1/api
..  _registering: https://pim.itemmaster.com
..  _`type hinting`: https://www.python.org/dev/peps/pep-0526
..  _`Health Canada`: http://www.hc-sc.gc.ca
..  _`kosher certifications`: https://pim.itemmaster.com/kosher_info