from copy import copy


class Status:

    def __init__(self, code, name):
        # type: (int, str) -> None
        self.code = code
        self.name = name


class ResponseError(Exception):

    status = Status(0, '')
    reason = ''
    responsibility = ''

    def __init__(self):
        super().__init__(
            '%s %s: %s\nReason: %s\nResponsibility: %s' % (
                str(self.status.code),
                self.status.name,
                self.reason,
                self.responsibility,
            )
        )


class BadRequest(ResponseError):

    status = Status(400, 'Bad Request')
    reason = 'The API request was malformed, i.e. a required parameter was missing or a parameter value was invalid.'
    responsibility = 'yours'


class Forbidden	(ResponseError):

    status = Status(403, 'Forbidden')
    reason = 'HTTP Header credentials are missing or invalid.'
    responsibility = 'yours'


class NotFound(ResponseError):

    status = Status(404, 'Not Found')
    reason = 'The API service does not exist.'
    responsibility = 'yours'


class MethodNotAllowed(ResponseError):

    status = Status(405, 'Method Not Allowed')
    reason = 'The API service does not allow a call to this URI.'
    responsibility = 'yours'


class NotAcceptable(ResponseError):

    status = Status(406, 'Not Acceptable')
    reason = 'You have reached your daily limit for downloaded items.'
    responsibility = 'yours'


class InternalServerError(ResponseError):

    status = Status(500, 'Internal Server Error')
    reason = 'There was a bug in our code and the API call broke. If you see this, email customerservice@itemmaster.com.'
    responsibility = 'ours'


class ServiceUnavailable(ResponseError):

    status = Status(503, 'Service Unavailable')
    reason = 'The API server is down for temporary maintenance, try again in a few minutes.'
    responsibility = 'ours'


# A map of response codes to their corresponding exceptions

RESPONSE_CODES_EXCEPTIONS = {}

for k, v in copy(locals()).items():
    if (
        k[0] != '_' and
        isinstance(v, type) and
        hasattr(v, 'status') and
        hasattr(v, 'reason') and
        hasattr(v, 'responsibility')
    ):
        code_value = getattr(v, 'status')  # type: int
        if code_value > 0:
            RESPONSE_CODES_EXCEPTIONS[str(code_value)] = v
