import functools
from datetime import date
from http.client import HTTPResponse
from typing import Optional
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import Request, urlopen

from itemmaster.elements import Manufacturers, Brands, Items, Retailers

HOST = 'api.itemmaster.com'
VERSION = '2.1'
ORIGIN = 'https://%s/v%s/' % (HOST, VERSION)


class ItemMaster:

    def __init__(
        self,
        user: Optional[str]=None,
        password: Optional[str]=None
    ):
        self.user = user
        self.password = password

    def request(
        self,
        service,  # type: str
        path=None,  # type: Optional[str]
        parameters=None,  # type: Optional[Union[Dict, str, bytes]]
        timeout=None,  # type: Optional[int]
        echo=False  # type: bool
    ):
        # type: (...) -> HTTPResponse
        if hasattr(parameters, 'items'):
            parameters = urlencode(parameters, doseq=True)
        url = ORIGIN + service
        if path:
            url += ('' if path[0] == '/' else '/') + path
        if parameters:
            url += '?' + parameters
        request = Request(url, headers=dict(username=self.user, password=self.password))

        def request_text():
            return (
                ('\n%s: %s\n' % (request.get_method(), url)) +
                '\n'.join(
                    '%s: %s' % (k, v)
                    for k, v in request.header_items()
                ) + (
                    ('\n' + str(request.data, encoding='utf-8'))
                    if request.data is not None
                    else ''
                )
            )
        if echo:
            print(request_text())
        try:
            response = urlopen(
                request,
                **(
                    {} if timeout is None else
                    dict(timeout=timeout)
                )
            )
        except HTTPError as e:
            response_data = str(e.file.read(), 'utf-8')
            e.msg = (
                'Request:\n' +
                request_text() +
                '\n\nResponse:\n' +
                response_data +
                '\n\n' +
                e.msg
            )
            raise e
        return response

    @functools.lru_cache()
    def manufacturers(
        self,
        manufacturer_id=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> Manufacturers
        response = self.request(
            'manufacturer',
            path=manufacturer_id,
            echo=echo
        )
        return Manufacturers(response)

    @functools.lru_cache()
    def brands(
        self,
        brand_id=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> Brands
        response = self.request(
            'brand',
            path=brand_id,
            echo=echo
        )
        return Brands(response)

    @functools.lru_cache()
    def retailers(
        self,
        retailer_id=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> Brands
        response = self.request(
            'retailer',
            path=retailer_id,
            echo=echo
        )
        return Retailers(response)

    @functools.lru_cache(maxsize=1)
    def items(
        self,
        item_id=None,  # type: Optional[str]
        idx=None,  # type: Optional[int]
        limit=None,  # type: Optional[int]
        q=None,  # type: Optional[str]
        m=None,  # type: Optional[str]
        b=None,  # type: Optional[str]
        upc=None,  # type: Optional[str]
        since=None,  # type: Optional[date]
        until=None,  # type: Optional[date]
        pi=None,  # type: Optional[str]
        ptga=None,  # type: Optional[str]
        pip=None,  # type: Optional[str]
        ef=None,  # type: Optional[str]
        eip=None,  # type: Optional[str]
        epl=None,  # type: Optional[Union[str, Tuple[str]]]
        epf=None,  # type: Optional[Union[str, Tuple[str]]]
        epr=None,  # type: Optional[Union[str, Tuple[str]]]
        timeout=None,  # type: Optional[int]
        echo=False  # type: bool
    ):
        # type: (...) -> Items
        """
        Retrieve a subset of the items available in ItemMaster's database which match given parameters.
        
        Parameters:
        
            - item_id (str)
            - idx (int): The index of the first item to return. This parameter is for use in conjunction with the 
            `limit` to iteratively retrieve a limited subset of the items found matching your other parameters.
            - limit (int): The number of items to return.
            - q (str): Retrieve all items with this string in the description.
            - m (str): Retrieve all items associated with this Manufacturer_ ID.
            - b (str): Retrieve all items associated with this Brand_ ID.
            - upc (str): Retrieve items associated with this UPC (GTIN).
            - since (date): Retrieve items updated on or after this `date`.
            - until (date): Retrieve items updated on or before this date.
            - pi (str): Specifies if the planogram images returned should be the original (unedited) or the edited. 
              Defaults to no images.
              + "o": Original
              + "c": Edited
              + "b": Both
            - ptga (int): Specifies the file size for TGA-16 planogram images, in kilobytes. Defaults to 100KB. The options
              available are 50 (50KB) or 100 (100KB).
            - pip (int): The resulting PPI of the planogram images. Specify only if you would like a different/lower PPI than
              the original image.
            - ef (str): The resulting format of the ecommerce images. The options available are "jpg", "png", or "tif".
            - eip (int): The resulting PPI of the ecommerce images.
            - epl (Tuple[int] or int): The resulting size(s) of the ecommerce left image. All ItemMaster ecommerce images
              are square—so, for example—passing a value of `50` will cause the ecommerce image returned to be re-sized to 50 x 50
              pixels. This can be 50, 100, 150, 200, 600, 900, 1000, or a  `tuple` of 2 or more of these values.
            - epf (Tuple[int] or int): The resulting size(s) of the ecommerce front image. All ItemMaster ecommerce images
              are square—so, for example—passing a value of `50` will cause the ecommerce image returned to be re-sized to 50 x 50
              pixels. This can be 50, 100, 150, 200, 600, 900, 1000, or a `tuple` of 2 or more of these values.
            - epr (int): The resulting size(s) of the ecommerce right image. All ItemMaster ecommerce images
              are square—so, for example—passing a value of `50` will cause the ecommerce image returned to be re-sized to 50 x 50
              pixels. This can be 50, 100, 150, 200, 600, 900, 1000, or a `tuple` of 2 or more of these values.
        """
        parameters = {}
        if idx is not None:
            parameters['idx'] = idx
        if limit is not None:
            parameters['limit'] = limit
        if q is not None:
            parameters['q'] = q
        if m is not None:
            parameters['m'] = q
        if b is not None:
            parameters['b'] = b
        if upc is not None:
            parameters['upc'] = upc
        if since is not None:
            parameters['since'] = since.strftime('%Y%m%d')
        if until is not None:
            parameters['until'] = until.strftime('%Y%m%d')
        if pi is not None:
            if pi not in ('c', 'o', 'b'):
                raise ValueError(
                    'The value provided for parameter `pi` is not valid: %s. ' % repr(pi) +
                    'Valid values include: "c" (edited), "o" (original) and "b" (both).'
                )
            parameters['pi'] = pi
        if ptga is not None:
            if ptga not in (50, 100):
                raise ValueError(
                    'The value provided for parameter `ptga` is not valid: %s. ' % repr(ptga) +
                    'Valid values include: 50 (50KB) or 100 (100KB).'
                )
            parameters['ptga'] = ptga
        if pip is not None:
            parameters['pip'] = pip
        if ef is not None:
            ef = ef.lower()
            if ef not in ('jpg', 'png', 'tif'):
                raise ValueError(
                    'The value provided for parameter `ef` is not valid: %s. ' % repr(ef) +
                    'Valid values include: "jpg", "png", or "tif".'
                )
            parameters['ef'] = ef
        if eip is not None:
            parameters['eip'] = eip
        if epl is not None:
            for r in (epl if isinstance(epl, tuple) else (epl,)):
                if r not in (50, 100, 150, 200, 600, 900, 1000):
                    raise ValueError(
                        'The value provided for parameter `epl` is not valid: %s. ' % repr(r) +
                        'Valid values include: 50, 100, 150, 200, 600, 900, or 1000.'
                    )
            parameters['epl'] = epl
        if epf is not None:
            for r in (epf if isinstance(epf, tuple) else (epf,)):
                if r not in (50, 100, 150, 200, 600, 900, 1000):
                    raise ValueError(
                        'The value provided for parameter `epf` is not valid: %s. ' % repr(r) +
                        'Valid values include: 50, 100, 150, 200, 600, 900, or 1000.'
                    )
            parameters['epf'] = epf
        if epr is not None:
            for r in (epr if isinstance(epr, tuple) else (epr,)):
                if r not in (50, 100, 150, 200, 600, 900, 1000):
                    raise ValueError(
                        'The value provided for parameter `epr` is not valid: %s. ' % repr(r) +
                        'Valid values include: 50, 100, 150, 200, 600, 900, or 1000.'
                    )
            parameters['epr'] = epr
        response = self.request(
            'item',
            path=item_id,
            parameters=parameters or None,
            timeout=timeout,
            echo=echo
        )
        return Items(response)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
