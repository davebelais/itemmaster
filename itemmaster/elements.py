import re
from base64 import b64decode, b64encode
from collections import OrderedDict
from copy import deepcopy
from datetime import datetime, date
from http.client import HTTPResponse
from itertools import chain
from typing import List
from typing import Optional
from xml.etree.ElementTree import Element, XML, tostring, ParseError

from decimal import Decimal, InvalidOperation


class UnknownAttribute(Warning):

    pass


class UnknownValue(Warning):

    pass


_DATE_OFFSET_RE = re.compile(r'([\-+]\d\d):?(\d\d)$')
_DATE_DECIMAL_RE = re.compile(r'(\.\d\d\d\d\d\d)\d+')


def str2datetime(s):
    # type: (str) -> datetime
    """
    Convert an XML *dateTime* string to a python *datetime* object.

    >>> print(str2datetime('2001-10-26T21:32:52+02:00'))
    2001-10-26 21:32:52+02:00

    >>> print(str2datetime('2016-03-28T23:33:41.3116627-0500'))
    2016-03-28 23:33:41.311662-05:00
    """
    dtf = "%Y-%m-%d"
    if 'T' in s:
        dtf += "T%H:%M:%S"
        if '.' in s:
            dtf += '.%f'
            s = _DATE_DECIMAL_RE.sub(
                r'\1',
                s
            )
    if s[-1] in 'Zz':
        s = s[:-1]
    if _DATE_OFFSET_RE.search(s):
        dtf += "%z"
        s = _DATE_OFFSET_RE.sub(
            r'\1\2',
            s
        )
    return datetime.strptime(
        s,
        dtf
    )


def str2date(s: str):
    """
    Convert an XML *date* string to a python *date* object.

    >>> print(str2date('2001-10-26'))
    2001-10-26
    """
    d = str2datetime(s)
    return date(
        year=d.year,
        month=d.month,
        day=d.day
    )


def datetime2str(dt):
    # type: (datetime) -> str
    s = dt.strftime('%Y-%m-%dT%H:%M:%S%z')
    return s


def date2str(d):
    # type: (date) -> str
    s = d.strftime('%Y-%m-%d')
    return s


TYPES_BASES = OrderedDict()


def bases(t):
    # type: (type) -> Set[type]
    """
    Retrieves a set containing all classes (`type` instances) from which the given class inherits.

        :param t: An instance type.

        :return: A set containing all types from which this type inherits (directly and indirectly).
    """
    if t not in TYPES_BASES:
        bs = set()
        for b in t.__bases__:
            for bb in bases(b):  # type: Set[type]
                bs.add(bb)
            bs.add(b)
        TYPES_BASES[t] = bs
    return TYPES_BASES[t]


def space_name(s):
    # type: (str) -> str
    """
    This function returns the un-qualified name of an XML element or attribute.

    :param s:
        An tag or attribute name.
    :return:
        The unqualified name of the XML element or attribute.
    :rtype:
        str
    """
    return re.match(
        r'^(?:\{(.*?)\})?(.*)$',
        s
    ).groups()


class SOAPElement(object):

    """
    This is a base class for building objects to represent SOAP XML elements.

    Child classes should each have the following properties:

        elements_properties:

            This should be an `OrderedDict` object which maps sub-element XML tag names (not including a name space) to
            a tuple containing the corresponding property name + type.

        xmlns:

            The full URL of the element's default name space.

    Child classes should initialize each of the properties from their static `elements_properties` property in their
    `__init__` method with a value of `None` (for properties corresponding to elements which can occur only once) or an
    empty list (for properties which correspond to elements which can occur more than once).
    """

    elements_properties = OrderedDict([])  # type: Dict[str, Sequence[str, type]]

    xmlns = None

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]],
        tag=None  # type: Optional[str]
    ):
        # type: (...) -> None
        """
        Initializes a `SOAPElement` instance, optionally deriving property values from provided XML data.

        :param xml:

            (optional) An instance of `str` or `xml.etree.ElementTree.Element` which represents an XML element, or an 
            `HTTPResponse` object containing the same information.
            If provided, the tag name of the given element will override the `tag` parameter below.

        :param tag:

            (optional) The tag name of the element (not including the name space). This is needed only if no
            `xml` is provided.
        """
        self._string = None
        self._element = None
        self.response = None
        self.tag = tag or self.__class__.__name__
        if xml is not None:
            namespace = None
            if isinstance(xml, str):
                self._string = xml
                try:
                    self._element = XML(xml)
                except ParseError as e:
                    e.args = tuple(chain(
                        (
                            ((e.args[0] + '\n\n') if e.args else '') +
                            xml
                        ),
                        e.args[1:] if e.args and len(e.args) > 1 else tuple()
                    ))
                    raise e
                namespace, self.tag = space_name(self._element.tag)
            elif isinstance(xml, Element):
                self._element = xml
                namespace, self.tag = space_name(self._element.tag)
            elif hasattr(xml, 'read'):
                self.response = xml
                self._string = str(xml.read(), 'utf-8', errors='ignore')
                try:
                    self._element = XML(self._string)
                except ParseError as e:
                    e.args = tuple(chain(e.args, (self._string,)))
                    raise e
                namespace, self.tag = space_name(self._element.tag)
            else:
                raise TypeError(xml)
            if self.elements_properties is not None:
                namespaces_prefixes = {}
                for a, p_t in self.elements_properties.items():
                    if a[:7] == '@xmlns:':
                        prefix = a[7:]
                        p, t = p_t
                        v = getattr(self, p)
                        if v is not None:
                            namespaces_prefixes[prefix] = v
                            if v == namespace:
                                self.tag = '%s:%s' % (prefix, self.tag)
                tags_elements_properties_types = []
                for a, v in self._element.attrib.items():
                    a_ns, a = space_name(a)
                    if a_ns in namespaces_prefixes:
                        nsp_a = '%s:%s' % (namespaces_prefixes[a_ns], a)
                        if nsp_a in self.elements_properties:
                            a = nsp_a
                    tn = '@' + a
                    if tn in self.elements_properties:
                        p, t = self.elements_properties[tn]
                        tags_elements_properties_types.append((tn, v, p, t))
                    else:
                        raise UnknownAttribute('`%s` is not a recognized attribute of `%s`:\n\n%s' % (
                            a,
                            self.__class__.__name__,
                            (self._string or tostring(self._element, encoding='unicode'))
                        ))
                v = self._element.text
                if v:
                    if '.' in self.elements_properties:
                        p, t = self.elements_properties['.']
                        tags_elements_properties_types.append(
                            ('.', v, p, t)
                        )
                    elif v.strip():
                        raise UnknownAttribute(
                            'No mapping was found for the text content of a `%s` element.' % self.tag
                        )
                for element in self._element:
                    ns, tn = space_name(element.tag)
                    if tn in self.elements_properties:
                        p, t = self.elements_properties[tn]
                        tags_elements_properties_types.append((tn, element, p, t))
                    else:
                        success = True
                        for e_a, e_v in element.attrib.items():
                            ns_a, e_a = space_name(e_a)
                            tn_a = '%s@%s' % (tn, e_a)
                            if tn_a in self.elements_properties:
                                p, t = self.elements_properties[tn_a]
                                tags_elements_properties_types.append((tn_a, e_v, p, t))
                            else:
                                success = False
                                break
                        for sub_element in element:
                            se_ns, se_tn = space_name(sub_element.tag)
                            se_tn = '%s/%s' % (tn, se_tn)
                            if se_tn in self.elements_properties:
                                p, t = self.elements_properties[se_tn]
                                tags_elements_properties_types.append((se_tn, sub_element, p, t))
                            else:
                                success = False
                                break
                        if not success:
                            if '*' in self.elements_properties:
                                p, t = self.elements_properties['*']
                                if SOAPElement in bases(t):
                                    tags_elements_properties_types.append((tn, element, p, t))
                                else:
                                    raise UnknownAttribute(
                                        '`%s` is not a recognized child element of `%s`' % (tn, self.__class__.__name__)
                                    )
                            else:
                                raise UnknownAttribute(
                                    '`%s` is not a recognized child element of `%s`' % (tn, self.__class__.__name__)
                                )
                for tn, v, p, t in tags_elements_properties_types:
                    if isinstance(t, (tuple, list)):
                        t = t[0] if t else None
                        if SOAPElement in bases(t):
                            if isinstance(v, Element):
                                getattr(self, p).append(t(v))
                            else:
                                raise ValueError(v)
                        else:
                            if t is Element:
                                if not isinstance(v, (Element, SOAPElement)):
                                    raise ValueError(v)
                                getattr(self, p).append(v)
                            else:
                                if isinstance(v, Element):
                                    v = v.text
                                if t is bool:
                                    getattr(self, p).append(
                                        True if v.lower().strip() in ('true', '1', 'yes', 'y')
                                        else False
                                    )
                                elif t is int:
                                    try:
                                        v = int(Decimal(v))
                                    except InvalidOperation as e:
                                        error_message = (
                                            '%s.%s: %s could not be cast as an integer\n\n%s' % (
                                                self.__class__.__name__,
                                                p,
                                                repr(v),
                                                self._string or tostring(self._element)
                                            ) + (
                                                (
                                                    '\n\n' + (
                                                        e.args[0]
                                                        if isinstance(e.args[0], str)
                                                        else repr(e.args[0])
                                                    )
                                                ) if e.args else ''
                                            )
                                        )
                                        e.args = tuple(chain(
                                            (error_message,),
                                            e.args[1:] if e.args else tuple()
                                        ))
                                        raise e
                                    getattr(self, p, v).append(v)
                                elif t is date:
                                    getattr(self, p).append(str2date(v))
                                elif t is datetime:
                                    getattr(self, p).append(str2datetime(v))
                                elif t is bytes:
                                    getattr(self, p).append(b64decode(v))
                                else:
                                    getattr(self, p).append(t(v))
                    else:
                        if SOAPElement in bases(t):
                            if isinstance(v, (Element, SOAPElement)):
                                setattr(self, p, t(v))
                            else:
                                raise ValueError(
                                    '%s is not a valid value for `%s.%s`' % (
                                        repr(v),
                                        self.__class__.__name__,
                                        p
                                    )
                                )
                        else:
                            if t is Element:
                                if not isinstance(v, Element):
                                    raise ValueError(v)
                                setattr(self, p, v)
                            else:
                                if isinstance(v, Element):
                                    v = v.text
                                if isinstance(v, str):
                                    v = v.strip()
                                    if v == '':
                                        continue
                                if t is bool:
                                    setattr(
                                        self,
                                        p,
                                        True if v.lower().strip() in ('true', '1', 'yes', 'y') else False
                                    )
                                elif t is int:
                                    try:
                                        v = int(Decimal(v))
                                    except InvalidOperation as e:
                                        error_message = (
                                            '%s.%s: %s could not be cast as an integer\n\n%s' % (
                                                self.__class__.__name__,
                                                p,
                                                repr(v),
                                                self._string or tostring(self._element)
                                            ) + (
                                                (
                                                    '\n\n' + (
                                                        e.args[0]
                                                        if isinstance(e.args[0], str)
                                                        else repr(e.args[0])
                                                    )
                                                ) if e.args else ''
                                            )
                                        )
                                        e.args = tuple(chain(
                                            (error_message,),
                                            e.args[1:] if e.args else tuple()
                                        ))
                                        raise e
                                    setattr(self, p, v)
                                elif t is date:
                                    setattr(self, p, str2date(v))
                                elif t is datetime:
                                    setattr(self, p, str2datetime(v))
                                elif t is bytes:
                                    setattr(self, p, b64decode(v))
                                else:
                                    try:
                                        setattr(self, p, t(v))
                                    except TypeError as e:
                                        e.args = tuple(
                                            [
                                                '\n%s: %s' % (t.__name__, repr(e.args[0])) + (
                                                    '\n' + e.args[0] if e.args else ''
                                                )
                                            ] + (
                                                list(e.args[1:])
                                                if len(e.args) > 1
                                                else []
                                            )
                                        )
                                        raise e

    def __str__(self):
        # type: () -> str
        """
        Casting instances of this class as `str` objects returns a normalized XML representation of the object, suitable
        for comparison, hashing, or passing in a SOAP request.

        :return:

            A normalized XML representation of the `SOAPElement` instance.
        """
        e = self.tag
        attributes_values = []
        if self.xmlns is not None:
            attributes_values.append(('xmlns', self.xmlns))
        for a, p_t in self.elements_properties.items():
            if a and a[0] == '@':
                a = a[1:]
                p, t = p_t
                vs = getattr(self, p)
                if vs is None:
                    continue
                if isinstance(t, (tuple, list)):
                    if t:
                        t = t[0]
                    else:
                        t = None
                else:
                    vs = [vs]
                for v in vs:
                    if not isinstance(v, t):
                        raise TypeError(
                            '%s is not an instance of `%s` (encountered while parsing `%s.%s`)' % (
                                repr(v),
                                t.__name__,
                                self.__class__.__name__,
                                p
                            )
                        )
                    if isinstance(v, datetime):
                        v = datetime2str(v)
                    elif isinstance(v, date):
                        v = date2str(v)
                    elif isinstance(v, bool):
                        v = 'true' if v else 'false'
                    elif isinstance(v, bytes):
                        v = str(b64encode(v), encoding='ascii')
                    elif isinstance(v, str):
                        if '"' in v:
                            v = v.replace('"', r'\"')
                        if '&' in v:
                            v = v.replace('&', '&amp;')
                        if '<' in v:
                            v = v.replace('<', '&lt;')
                        if '>' in v:
                            v = v.replace('>', '&gt;')
                    else:
                        v = str(v)
                    attributes_values.append((a, v))
        s = [
            '<%s%s>' % (
                self.tag,
                ''.join(
                    ' %s="%s"' % (a, v)
                    for a, v in attributes_values
                )
            )
        ]
        for tn, p_t in self.elements_properties.items():
            if tn and (tn[0] == '@' or tn == '*'):
                continue
            p, t = p_t
            vs = getattr(self, p)
            if isinstance(t, (tuple, list)):
                if t:
                    t = t[0]
                else:
                    t = None
            else:
                vs = [] if vs is None else [vs]
            if vs is None:
                raise ValueError(
                    '`None` is not a valid value for `%s.%s`' % (
                        self.__class__.__name__,
                        p
                    )
                )
            for v in vs:
                if v is not None:
                    if not (
                        (isinstance(v, SOAPElement) and t is Element) or
                        isinstance(v, t)
                    ):
                        raise TypeError(
                            '`%s.%s`: `%s` is not an instance of `%s`' % (
                                self.__class__.__name__,
                                p,
                                repr(v),
                                t.__name__
                            )
                        )
                if isinstance(v, SOAPElement):
                    if tn == '.':
                        raise ValueError(
                            '`%s.%s`: A `SOAPElement` cannot be attributed to the text content of an element.' % (
                                self.__class__.__name__,
                                p
                            )
                        )
                else:
                    if isinstance(v, Element):
                        v = tostring(v, encoding='unicode')
                    elif isinstance(v, datetime):
                        v = datetime2str(v)
                    elif isinstance(v, date):
                        v = date2str(v)
                    elif isinstance(v, bool):
                        v = 'true' if v else 'false'
                    elif isinstance(v, bytes):
                        v = str(b64encode(v), encoding='ascii')
                    elif isinstance(v, str):
                        if '&' in v:
                            v = v.replace('&', '&amp;')
                        if '<' in v:
                            v = v.replace('<', '&lt;')
                        if '>' in v:
                            v = v.replace('>', '&gt;')
                    else:
                        v = str(v)
                if tn == '.':
                    s.append(v)
                else:
                    for tn in reversed(tn.split('/')):
                        if isinstance(v, SOAPElement):
                            v = str(v)
                        elif '@' in tn:
                            tn, ta = tn.split('@')
                            v = '<%(tn)s %(ta)s="%(v)s" />' % dict(
                                tn=tn,
                                ta=ta,
                                v=v
                            )
                        else:
                            v = '<%(tn)s>%(v)s</%(tn)s>' % dict(
                                tn=tn,
                                v=v
                            )
                    s.append(v)
        if '*' in self.elements_properties:
            for se in self._element:
                ns, tn = space_name(se.tag)
                if tn not in self.elements_properties:
                    s.append(str(se))
        s.append('</%s>' % e)
        return ''.join(s)

    def __bool__(self):
        # type: () -> bool
        return True

    def __eq__(self, other):
        # type: (object) -> bool
        return (
            hasattr(other, '__class__') and
            (self.__class__ is other.__class__) and
            str(self) == str(other)
        )

    def __ne__(self, other):
        # type: (object) -> bool
        return (
            False if self == other
            else True
        )

    def __hash__(self):
        return hash(str(self))

    def __copy__(self):
        nse = self.__class__(self._string or self._element)
        nse.response = self.response
        for p, t in self.elements_properties.values():
            setattr(nse, p, getattr(self, p))
        return nse

    def __deepcopy__(
        self,
        memo=None  # type: Optional[dict]
    ):
        nse = self.__class__(
            self._string or
            deepcopy(self._element) if self._element is not None
            else None
        )
        nse.response = deepcopy(self.response, memo=memo)
        for p, t in self.elements_properties.values():
            setattr(nse, p, deepcopy(getattr(self, p), memo=memo))
        return nse


class CompanyTrackingID(SOAPElement):
    """
    Properties:
    
        - company_id (str)
        - company_name (str)
    """

    elements_properties = OrderedDict([
        ('@companyId', ('company_id', str)),
        ('@companyName', ('company_name', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='companyTrackingId',  # type: str
    ):
        self.company_id = None  # type: Optional[str]
        self.company_name = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class UPC(SOAPElement):
    """
    Properties:

        - type (str): "UPC" or "PLU".
        - text (str): A universal product code.
    """

    elements_properties = OrderedDict([
        ('@type', ('type', str)),
        ('.', ('text', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='companyTrackingId',  # type: str
    ):
        self.type = None  # type: Optional[str]
        self.text = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class ValueWithID(SOAPElement):
    """
    Properties:

        - id (str)
        - text (str)
    """

    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('.', ('text', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
    ):
        self.id = None  # type: Optional[str]
        self.text = None  # type: Optional[str]
        super().__init__(xml=xml)


class Category(SOAPElement):
    """
    Properties:

        - type (str): Currently, the only taxonomy is "GS1".
        - id (str): The category's unique ID within the taxonomy specified in the `type` property.
        - name (str): The name of the category.
    """

    elements_properties = OrderedDict([
        ('@type', ('type', str)),
        ('@id', ('id', str)),
        ('.', ('text', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='companyTrackingId',  # type: str
    ):
        self.type = None  # type: Optional[str]
        self.id = None  # type: Optional[str]
        self.text = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class XFormSource(SOAPElement):
    """
    Properties:

        - id (str): Identifies the creator of a record.
        - datetime (datetime): The date/time at which a record was created.
    """

    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('.', ('datetime', datetime)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='created',  # type: str
    ):
        self.id = None  # type: Optional[str]
        self.datetime = None  # type: Optional[datetime]
        super().__init__(xml=xml, tag=tag)


class Medium(SOAPElement):

    """
    Properties:

        - description (str): A description of the referenced media.
        - url (str): The URL where this media can be accessed.
        - type (str): "edited" or "unedited"
        - view (str): A `GDSN product view code 
          <http://www.gs1.org/sites/default/files/docs/gdsn/Product_Image_Specification.pdf>`.
        - mime_type (str): `The type of media <https://en.wikipedia.org/wiki/Media_type>`.
        - image_source (str): An identifier representing the source of the image.
        - added (datetime): The date/time when the medium was added.
    """

    elements_properties = OrderedDict([
        ('@type', ('type', str)),
        ('@view', ('view', str)),
        ('@mimeType', ('mime_type', str)),
        ('@imageSource', ('image_source', str)),
        ('@added', ('added', datetime)),
        ('description', ('description', str)),
        ('url', ('url', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='medium',  # type: str
    ):
        self.description = None  # type: Optional[str]
        self.url = None  # type: Optional[str]
        self.type = None  # type: Optional[str]
        self.view = None  # type: Optional[str]
        self.mime_type = None  # type: Optional[str]
        self.image_source = None  # type: Optional[str]
        self.added = None  # type: Optional[datetime]
        super().__init__(xml=xml, tag=tag)


class ManufacturerSuppliedContent(SOAPElement):
    """
    Properties:
    
        - description (str): A description of the referenced media.
        - url (str): A URL from where this media can be retrieved.
        - file_type (str): Example: "customMobile" or "customImage".
        - mime_type (str): `The type of media <https://en.wikipedia.org/wiki/Media_type>`.
        - added (datetime): When was this asset received from the supplier?
        - copy (str): The text content of the image.
    """

    elements_properties = OrderedDict([
        ('@fileType', ('file_type', str)),
        ('@mimeType', ('mime_type', str)),
        ('@added', ('added', datetime)),
        ('@description', ('description', str)),
        ('@copy', ('copy', str)),
        ('url', ('url', str)),
    ])

    def __init__(
        self,
        xml=None  # type: Optional[Union[Element, str, HTTPResponse]]
    ):
        # type: (...) -> None
        self.url = None  # type: Optional[str]
        self.file_type = None  # type: Optional[str]
        self.mime_type = None  # type: Optional[str]
        self.added = None  # type: Optional[datetime]
        self.description = None  # type: Optional[str]
        self.copy = None  # type: Optional[str]
        super().__init__(xml=xml)


class Measurement(SOAPElement):
    """
    Properties:

        - measure (Decimal): A numeric measurement.
        - uom (str): The unit in which the *measure* is expressed.
        
          + "BOX": # of boxes
          + "CT": count (# of units)
          + "CUP": # of cups
          + "FL OZ": fluid ounce
          + "FT": foot
          + "G": gram
          + "GAL": gallon
          + "IN": inch
          + "KIT": # of kits
          + "KG": kilogram
          + "KT": # of kits
          + "L": liter
          + "LB": pound
          + "MG": milligram
          + "ML": milliliter
          + "OZ": ounce
          + "PACK": # of packs
          + "PACKAGE": # of packs
          + "PIECE(S)": # of peices in a set
          + "PR": pair
          + "PT": pint
          + "QT": quart
          + "SQ FT": square foot
          + "SQ IN": square inch
          + "TBS": tablespoon
          + "YARDS": yard
          + "BAR": This unit of measure is usually accompanied by an empty value and should be ignored
    """

    elements_properties = OrderedDict([
        ('measure', ('measure', Decimal)),
        ('uom', ('uom', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='measurement'  # type: str
    ):
        # type: (...) -> None
        self.measure = None  # type: Optional[Decimal]
        self.uom = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class ServingSize(SOAPElement):
    """
    Properties:

        - measure (Decimal): A numeric measurement.
        - uom (str): The unit of measure.
        - type (str): "us" or "metric"
    """

    elements_properties = OrderedDict([
        ('@type', ('type', str)),
        ('measure', ('measure', Decimal)),
        ('uom', ('uom', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='servingSize'  # type: str
    ):
        # type: (...) -> None
        self.type = None  # type: Optional[str]
        self.measure = None  # type: Optional[Decimal]
        self.uom = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class PackageData(SOAPElement):
    """
    Properties
    
        - type (str): Type of unit (such as "Item" or "Case").
        - approximation (bool): Are these measurements approximated?
        - length (Measurement): Package length/depth—see the `GS1 package measurement
          rules <http://www.gs1.org/docs/gdsn/3.1/GDSN_Package_Measurement_Rules.pdf>`_.
        - height (Measurement): Package height—see the `GS1 package measurement rules
          <http://www.gs1.org/docs/gdsn/3.1/GDSN_Package_Measurement_Rules.pdf>`_.
        - width (Measurement): Package width—see the `GS1 package measurement rules
          <http://www.gs1.org/docs/gdsn/3.1/GDSN_Package_Measurement_Rules.pdf>`_.
        - weight (`Measurement`_): The gross weight of the product, including the packaging.
        - units_in_package (int): The number of units in each package.
        - package_type (ValueWithID): The type of packaging unit. Valid values include:
          "PEN", "OVERWRAP", "STICK", "POLYBAG", "BLISTER CARD", "CASE PACK", "ROLL",
          "BLISTER PACK", "BLISTER SUSTAINABLE PACK", "PLASTIC CLAMSHELL", "UNIT",
          "RESEALABLE BAG", "SHRINKWRAP", "LOAF", "PUMP", "TRAY", "TUBE", "POT", "TUB",
          "TIN", "PACKET", "QT", "PINT", "PAN", "KEG", "LOG", "JUG", "JAR", "GALLON", "CUP",
          "BOWL", "NONE", "CARTON", "CAN", "BOTTLE", "BOX", "BIN", "BAR", "BAG", "STAND PACK",
          "CANISTER", "SLEEVE", "PLASTIC CONTAINER", "POUCH", "SPRAY CAN", "PACKAGE",
          "CONTAINER", "PACK", and "CARD".
        - package_size (Measurement): The size of the product, as described on the
          package.
        - net_weight (Measurement): Actual, computed, or estimated weight of the item
          without its container and/or packaging.
        - dry_weight (Measurement): The net weight of the product, when empty.
        - information (str)
        - recyclable (bool): Is this item recycleable?
    """

    elements_properties = OrderedDict([
        ('@type', ('type', str)),
        ('@approximation', ('approximation', bool)),
        ('length', ('length', Measurement)),
        ('height', ('height', Measurement)),
        ('width', ('width', Measurement)),
        ('weight', ('weight', Measurement)),
        ('unitsInPackage', ('units_in_package', int)),
        ('packageType', ('package_type', ValueWithID)),
        ('packageSize', ('package_size', Measurement)),
        ('dryWeight', ('dry_weight', Measurement)),
        ('netWeight', ('net_weight', Measurement)),
        ('information', ('information', str)),
        ('recyclable', ('recyclable', bool)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='packageData'  # type: str
    ):
        self.type = None  # type: Optional[str]
        self.approximation = None  # type: Optional[bool]
        self.length = None  # type: Optional[Measurement]
        self.height = None  # type: Optional[Measurement]
        self.width = None  # type: Optional[Measurement]
        self.weight = None  # type: Optional[Measurement]
        self.units_in_package = None  # type: Optional[int]
        self.package_type = None  # type: Optional[ValueWithID]
        self.package_size = None  # type: Optional[Measurement]
        self.dry_weight = None  # type: Optional[Measurement]
        self.net_weight = None  # type: Optional[Measurement]
        self.information = None  # type: Optional[str]
        self.recyclable = None  # type: Optional[bool]
        super().__init__(xml=xml, tag=tag)


class PlanogramData(SOAPElement):
    """
    Properties:

        - shelf_man_flag (bool)
        - depth_count (Decimal)
        - width_count (Decimal)
        - height_count (Decimal)
        - depth_nesting (Decimal)
        - vertical_nesting (Decimal)
        - display_depth (Decimal)
        - display_width (Decimal)
        - division_name (str)
        - dual_nesting (str)
        - multiple_shelf_facings (int)
        - peg_down (Decimal)
        - peg_right (Decimal)
        - tray_count (Decimal)
        - tray_depth (Decimal)
        - tray_width (Decimal)
        - tray_height (Decimal)
    """

    elements_properties = OrderedDict([
        ('@shelfManFlag', ('shelf_man_flag', bool)),
        ('depthCount', ('depth_count', Decimal)),
        ('widthCount', ('width_count', Decimal)),
        ('heightCount', ('height_count', Decimal)),
        ('depthNesting', ('depth_nesting', Decimal)),
        ('verticalNesting', ('vertical_nesting', Decimal)),
        ('displayDepth', ('display_depth', Decimal)),
        ('displayWidth', ('display_width', Decimal)),
        ('divisionName', ('division_name', Decimal)),
        ('dualNesting', ('dual_nesting', str)),
        ('multipleShelfFacings', ('multiple_shelf_facings', int)),
        ('pegDown', ('peg_down', Decimal)),
        ('pegRight', ('peg_right', Decimal)),
        ('trayCount', ('tray_count', Decimal)),
        ('trayDepth', ('tray_depth', Decimal)),
        ('trayWidth', ('tray_width', Decimal)),
        ('trayHeight', ('tray_height', Decimal)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='planogramData'  # type: str
    ):
        self.shelf_man_flag = None  # type: Optional[bool]
        self.depth_count = None  # type: Decimal
        self.width_count = None  # type: Decimal
        self.height_count = None  # type: Optional[Decimal]
        self.depth_nesting = None  # type: Optional[Decimal]
        self.vertical_nesting = None  # type: Optional[Decimal]
        self.display_depth = None  # type: Optional[Decimal]
        self.display_width = None  # type: Optional[Decimal]
        self.division_name = None  # type: Optional[str]
        self.dual_nesting = None  # type: Optional[str]
        self.multiple_shelf_facings = None  # type: Optional[int]
        self.peg_down = None  # type: Optional[Decimal]
        self.peg_right = None  # type: Optional[Decimal]
        self.tray_count = None  # type: Optional[Decimal]
        self.tray_depth = None  # type: Optional[Decimal]
        self.tray_width = None  # type: Optional[Decimal]
        self.tray_height = None  # type: Optional[Decimal]
        super().__init__(xml=xml, tag=tag)


class NutritionalClaims(SOAPElement):
    """
    Properties:

        - fat_free (bool)
        - good_source_of_fiber (bool)
        - low_fat (bool)
        - low_sodium (bool)
        - sugar_free (bool)
    """

    elements_properties = OrderedDict([
        ('fatFree', ('fat_free', bool)),
        ('goodSourceOfFiber', ('good_source_of_fiber', bool)),
        ('lowFat', ('low_fat', bool)),
        ('lowSodium', ('low_sodium', bool)),
        ('sugarFree', ('sugar_free', bool)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='nutritionalClaims'  # type: str
    ):
        self.fat_free = None  # type: Optional[bool]
        self.good_source_of_fiber = None  # type: Optional[bool]
        self.low_fat = None  # type: Optional[bool]
        self.low_sodium = None  # type: Optional[bool]
        self.sugar_free = None  # type: Optional[bool]
        super().__init__(xml=xml, tag=tag)


class ContainerRefund(SOAPElement):
    """
    Properties:

        - local (str): An abbreviation representing the locale/state.
        - amount (str): The deposit amount.
    """

    elements_properties = OrderedDict([
        ('local', ('local', str)),
        ('amount', ('amount', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='nutritionalClaims'  # type: str
    ):
        self.local = None  # type: Optional[str]
        self.amount = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class FoodRelatedIndicators(SOAPElement):
    """
    Properties:

        - temperature_indicator (str): "Frozen", "Chilled", "Perishable", or "Shelf Stable".
        - vegan (bool)
        - vegetarian (bool)
        - lactose_free (bool)
        - flavor (bool): Indicates whether the product contains artificial flavoring.
        - antibiotic_free (bool)
        - wheat_free (bool)
        - gluten_free (bool)
        - hormone_free (bool)
        - natural (bool)
        - nitrates_free (bool)
        - nitrites_free (bool)
        - organic (bool)
        - peanut_free (bool)
        - ready_to_cook (bool)
        - ready_to_heat (bool)
        - dairy_free (bool)
        - egg_free (bool)
        - non_gmo (bool)
        - kosher (bool)
        - shellfish_free (bool)
        - soy_free (bool)
        - milk_free (bool)
        - crustacean_free (bool)
        - fish_free (bool)
        - sesame_free (bool)
        - nut_free (bool)
        - halal (bool)
        - paleo (bool)
    """

    elements_properties = OrderedDict([
        ('temperatureIndicator', ('temperature_indicator', str)),
        ('vegan', ('vegan', bool)),
        ('vegetarian', ('vegetarian', bool)),
        ('lactoseFree', ('lactose_free', bool)),
        ('flavor', ('flavor', bool)),
        ('antibioticFree', ('antibiotic_free', bool)),
        ('wheatFree', ('wheat_free', bool)),
        ('glutenFree', ('gluten_free', bool)),
        ('hormoneFree', ('hormone_free', bool)),
        ('natural', ('natural', bool)),
        ('nitratesFree', ('nitrates_free', bool)),
        ('nitritesFree', ('nitrites_free', bool)),
        ('organic', ('organic', bool)),
        ('peanutFree', ('peanut_free', bool)),
        ('readyToCook', ('ready_to_cook', bool)),
        ('readyToHeat', ('ready_to_heat', bool)),
        ('dairyFree', ('dairy_free', bool)),
        ('eggFree', ('egg_free', bool)),
        ('nonGmo', ('non_gmo', bool)),
        ('kosher', ('kosher', bool)),
        ('shellfishFree', ('shellfish_free', bool)),
        ('soyFree', ('soy_free', bool)),
        ('milkFree', ('milk_free', bool)),
        ('crustaceanFree', ('crustacean_free', bool)),
        ('fishFree', ('fish_free', bool)),
        ('sesameFree', ('sesame_free', bool)),
        ('nutFree', ('nut_free', bool)),
        ('halal', ('halal', bool)),
        ('paleo', ('paleo', bool)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='foodRelatedIndicators'  # type: str
    ):
        self.vegan = None  # type: Optional[bool]
        self.vegetarian = None  # type: Optional[bool]
        self.lactose_free = None  # type: Optional[bool]
        self.flavor = None  # type: Optional[bool]
        self.antibiotic_free = None  # type: Optional[bool]
        self.temperature_indicator = None  # type: Optional[str]
        self.wheat_free = None  # type: Optional[bool]
        self.gluten_free = None  # type: Optional[bool]
        self.hormone_free = None  # type: Optional[bool]
        self.natural = None  # type: Optional[bool]
        self.nitrates_free = None  # type: Optional[bool]
        self.nitrites_free = None  # type: Optional[bool]
        self.organic = None  # type: Optional[bool]
        self.peanut_free = None  # type: Optional[bool]
        self.ready_to_cook = None  # type: Optional[bool]
        self.ready_to_heat = None  # type: Optional[bool]
        self.dairy_free = None  # type: Optional[bool]
        self.egg_free = None  # type: Optional[bool]
        self.non_gmo = None  # type: Optional[bool]
        self.kosher = None  # type: Optional[bool]
        self.shellfish_free = None  # type: Optional[bool]
        self.soy_free = None  # type: Optional[bool]
        self.milk_free = None  # type: Optional[bool]
        self.crustacean_free = None  # type: Optional[bool]
        self.fish_free = None  # type: Optional[bool]
        self.sesame_free = None  # type: Optional[bool]
        self.nut_free = None  # type: Optional[bool]
        self.halal = None  # type: Optional[bool]
        self.paleo = None  # type: Optional[bool]
        super().__init__(xml=xml, tag=tag)


class Nutrient(SOAPElement):
    """
    Properties:
    
        - text (str): A numeric measurement (expressed as text).
        - uom (str):
        
          + "kg": kilogram
          + "g": gram
          + "mg": milligram
          + "mcg": Microgram
          + "IU": international units
          + "PCT": percentage
          
        - daily_pct (str): % of daily value—potentially including modifiers such as "<" or "~", or a footnote reference 
          such as "*" or "**"a in addition to a numeric quantity.
        - calories (str): # of calories—potentially including modifiers such as "<" or "~", or a footnote reference 
          such as "*" or "**" in addition to a numeric quantity.
    """

    elements_properties = OrderedDict([
        ('.', ('text', str)),
        ('@uom', ('uom', str)),
        ('@dailyPct', ('daily_pct', str)),
        ('@calories', ('calories', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
    ):
        self.text = None  # type: Optional[str]
        self.uom = None  # type: Optional[str]
        self.calories = None  # type: Optional[str]
        self.daily_pct = None  # type: Optional[str]
        super().__init__(xml=xml)
        if self.uom not in ('kg', 'g', 'mg', 'mcg', 'IU', 'PCT'):
            raise UnknownValue(
                '%s is not a recognized value for Nutrient.uom' % repr(self.uom)
            )


class Nutrition(SOAPElement):
    """
    Properties:

        - sequence (int)
        - title (str): Header text on this nutrition panel.
        - number_served_in_package (str)
        - number_of_servings (str)
        - serving_sizes (Sequence[ServingSize]): A list of `ServingSize`_ measurements, all referencing the same amount, 
          but potentially quantified using differing units of measure. Typically, if more than one measurement is given, 
          one will be metric, and the other will be imperial (US).
        - serving_size_full_txt (str)
        - racc_amt (Measurement): A reference amount is a specific, regulated quantity of a type of food usually eaten 
          by an individual at one sitting, as established by Health Canada.
        - energy (int): The number of calories in a serving.
        - saturated_fat (Nutrient)
        - polyunsaturated_fat (Nutrient)
        - monounsaturated_fat (Nutrient)
        - trans_fat (Nutrient)
        - cholesterol (Nutrient)
        - sodium (Nutrient)
        - potassium (Nutrient)
        - carbohydrates (Nutrient)
        - dietary_fiber (Nutrient)
        - soluable_fiber (Nutrient)
        - insoluable_fiber (Nutrient)
        - sugars (Nutrient)
        - sugar_alchohol (Nutrient)
        - other_carbohydrates (Nutrient)
        - protein (Nutrient)
    
    The following properties describe the percentage of each nutrient contained in a serving, potentially including   
    descriptive modifiers composed of non-numeric digits such as "less than", "approximately", etc., in addition to 
    numeric quantities.
    
        - daily_percent_of_vitamin_a (str) 
        - daily_percent_of_vitamin_c (str)
        - daily_percent_of_calcium (str)
        - daily_percent_of_iron (str)
        - daily_percent_of_copper (str)
        - daily_percent_of_folic_acid (str)
        - daily_percent_of_iodine (str)
        - daily_percent_of_magnesium (str)
        - daily_percent_of_niacin (str)
        - daily_percent_of_phosphorous (str)
        - daily_percent_of_riboflavin (str)
        - daily_percent_of_thiamin (str)
        - daily_percent_of_vitamin_b12 (str)
        - daily_percent_of_vitamin_b6 (str)
        - daily_percent_of_vitamin_d (str)
        - daily_percent_of_vitamin_e (str)
        - daily_percent_of_vitamin_zinc (str)
    """

    elements_properties = OrderedDict([
        ('@sequence', ('sequence', int)),
        ('title', ('title', str)),
        ('numberServedInPackage', ('number_served_in_package', str)),
        ('numberOfServings', ('number_of_servings', str)),
        ('servingSizes/servingSize', ('serving_sizes', (ServingSize,))),
        ('servingSizeFullTxt', ('serving_size_full_txt', str)),
        ('raccAmt', ('racc_amt', Measurement)),
        ('energy', ('energy', int)),
        ('totalFat', ('total_fat', Nutrient)),
        ('saturatedFat', ('saturated_fat', Nutrient)),
        ('polyunsaturatedFat', ('polyunsaturated_fat', Nutrient)),
        ('monounsaturatedFat', ('monounsaturated_fat', Nutrient)),
        ('transFat', ('trans_fat', Nutrient)),
        ('cholesterol', ('cholesterol', Nutrient)),
        ('sodium', ('sodium', Nutrient)),
        ('potassium', ('potassium', Nutrient)),
        ('carbohydrates', ('carbohydrates', Nutrient)),
        ('dietaryFiber', ('dietary_fiber', Nutrient)),
        ('soluableFiber', ('soluable_fiber', Nutrient)),
        ('insoluableFiber', ('insoluable_fiber', Nutrient)),
        ('sugars', ('sugars', Nutrient)),
        ('sugarAlchohol', ('sugar_alchohol', Nutrient)),
        ('otherCarbohydrates', ('other_carbohydrates', Nutrient)),
        ('protein', ('protein', Nutrient)),
        ('dailyPercentOfVitaminA', ('daily_percent_of_vitamin_a', str)),
        ('dailyPercentOfVitaminC', ('daily_percent_of_vitamin_c', str)),
        ('dailyPercentOfCalcium', ('daily_percent_of_calcium', str)),
        ('dailyPercentOfIron', ('daily_percent_of_iron', str)),
        ('dailyPercentOfCopper', ('daily_percent_of_copper', str)),
        ('dailyPercentOfFolicAcid', ('daily_percent_of_folic_acid', str)),
        ('dailyPercentOfIodine', ('daily_percent_of_iodine', str)),
        ('dailyPercentOfMagnesium', ('daily_percent_of_magnesium', str)),
        ('dailyPercentOfNiacin', ('daily_percent_of_niacin', str)),
        ('dailyPercentOfPhosphorous', ('daily_percent_of_phosphorous', str)),
        ('dailyPercentOfRiboflavin', ('daily_percent_of_riboflavin', str)),
        ('dailyPercentOfThiamin', ('daily_percent_of_thiamin', str)),
        ('dailyPercentOfVitaminB12', ('daily_percent_of_vitamin_b12', str)),
        ('dailyPercentOfVitaminB6', ('daily_percent_of_vitamin_b6', str)),
        ('dailyPercentOfVitaminD', ('daily_percent_of_vitamin_d', str)),
        ('dailyPercentOfVitaminE', ('daily_percent_of_vitamin_e', str)),
        ('dailyPercentOfVitaminZinc', ('daily_percent_of_vitamin_zinc', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='nutrition'  # type: str
    ):
        # type: (...) -> None
        self.sequence = None  # type: Optional[int]
        self.title = None  # type: Optional[str]
        self.number_served_in_package = None  # type: Optional[str]
        self.number_of_servings = None  # type: Optional[str]
        self.serving_sizes = []  # type: List[ServingSize]
        self.serving_size_full_txt = None  # type: Optional[str]
        self.racc_amt = None  # type: Optional[Measurement]
        self.energy = None  # type: Optional[int]
        self.total_fat = None  # type: Optional[Nutrient]
        self.saturated_fat = None  # type: Optional[Nutrient]
        self.polyunsaturated_fat = None  # type: Optional[Nutrient]
        self.monounsaturated_fat = None  # type: Optional[Nutrient]
        self.trans_fat = None  # type: Optional[Nutrient]
        self.cholesterol = None  # type: Optional[Nutrient]
        self.sodium = None  # type: Optional[Nutrient]
        self.potassium = None  # type: Optional[Nutrient]
        self.carbohydrates = None  # type: Optional[Nutrient]
        self.dietary_fiber = None  # type: Optional[Nutrient]
        self.soluable_fiber = None  # type: Optional[Nutrient]
        self.insoluable_fiber = None  # type: Optional[Nutrient]
        self.sugars = None  # type: Optional[Nutrient]
        self.sugar_alchohol = None  # type: Optional[Nutrient]
        self.other_carbohydrates = None  # type: Optional[Nutrient]
        self.protein = None  # type: Optional[Nutrient]
        self.daily_percent_of_vitamin_a = None  # type: Optional[str]
        self.daily_percent_of_vitamin_c = None  # type: Optional[str]
        self.daily_percent_of_calcium = None  # type: Optional[str]
        self.daily_percent_of_iron = None  # type: Optional[str]
        self.daily_percent_of_copper = None  # type: Optional[str]
        self.daily_percent_of_folic_acid = None  # type: Optional[str]
        self.daily_percent_of_iodine = None  # type: Optional[str]
        self.daily_percent_of_magnesium = None  # type: Optional[str]
        self.daily_percent_of_niacin = None  # type: Optional[str]
        self.daily_percent_of_phosphorous = None  # type: Optional[str]
        self.daily_percent_of_riboflavin = None  # type: Optional[str]
        self.daily_percent_of_thiamin = None  # type: Optional[str]
        self.daily_percent_of_vitamin_b12 = None  # type: Optional[str]
        self.daily_percent_of_vitamin_b6 = None  # type: Optional[str]
        self.daily_percent_of_vitamin_d = None  # type: Optional[str]
        self.daily_percent_of_vitamin_e = None  # type: Optional[str]
        self.daily_percent_of_vitamin_zinc = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Grocery(SOAPElement):
    """
    Properties:

        - ndc_code (str): The "National Drug Code" is a unique, 10-digit, 3-segment number. It is a universal product 
          identifier for human drugs in the United States. The code is required to be present on all 
          over-the-counter and prescription medication packages and inserts in the United States.              
        - npn_code (str): The "Natural Product Number" is an 8-digit number issued for products which have 
          been evaluated and licensed for sale in Canada by 
          `Health Canada <http://www.hc-sc.gc.ca/dhp-mps/prodnatur/about-apropos/index-eng.php#a21>`.
        - din_code (str): The Drug Identification Number (DIN) is the 8 digit number located on the label of 
          prescription and over-the-counter drug products that have been evaluated by the Therapeutic Products 
          Directorate (TPD) and approved for sale in Canada.
        - alcohol_by_volume_percent (str): : A description of the percentage of alcohol in this beverage, 
          potentially including descriptive modifiers composed of non-numeric digits such as "less than", 
          "approximately", etc.
        - alcohol_type (str)
        - ingredients (str): A list of ingredients in this product, as described on the product packaging. For
          example: "Milk Chocolate (Sugar, Chocolate, Cocoa Butter, Skim Milk, Lactose, Milkfat, Soy Lecithin, 
          Artificial and Natural Flavors, Salt), Sugar, Corn Syrup, Hydrogenated Palm Kernel Oil, Milkfat, 
          Skim Milk, Cornstarch, Less Than 1% - Lactose, Dextrin, Salt, Mono and Diglycerides, Coloring (Includes 
          Blue 1 Lake, Yellow 6, Red 40, Yellow 5, Blue 1), Chocolate, Artificial Flavor, Gum Acacia.".
        - vitamins_and_minerals (str): A list of vitamins and minerals contained in this product, as described on  
          the packaging. For example: "Added Sugars-6g-12%, Vitamin D-0mcg-0%, Calcium-0mg-0%, Iron-0.84mg-4%."
        - nutritional_claims (NutritionalClaims): This identifies nutrition claims asserted on a product's 
          packaging.
        - kosher_codes (Sequence[str]): A list of `kosher certifications <https://pim.itemmaster.com/kosher_info>`
          applicable to this product. Valid values include:

          + 1: The Union of Orthodox Jewish Congregation
          + 2: The Organized Kashruth Laboratories
          + 3: Chicago Rabbinical Council
          + 4: Star-D Kosher Supervision of the National Council of the Young Israel
          + 5: Kosher Supervision of America
          + 6: KOF-K Kosher Supervision
          + 7: Star K Kosher Certification
          + 8: K'hal Adath Jeshuren, NY, NY
          + 9: Shofar Kosher Foods
          + 10: Rabbinical Council of New England, (KVH)
          + 11: Rabbi M. Small Chicago
          + 12: The Diamond K
          + 13: Dairy
          + 14: Triangle Kosher Certification
          + 15: Kosher all year including Passover
          + 16: Kosher Overseers
          + 17: Unknown Circle U Organization
          + 18: Scroll K/Vaad Hakashrus of Denver
          + 19: United Mehadrin Kosher
          + 20: Kashrut Division of the London Beth Din, Court of the Chief Rabbi
          + 21: Unknown Kosher Dairy Organization
          + 22: Capital District Vaad Hakashruth
          + 23: Vaad Hakashrus of Buffalo
          + 24: Unknown Kosher Certification
          + 25: Orthodox Jewish Council Kosher Technical Konsultants
          + 26: Rabbi Yechiel Babad (Tartikover Rav),
          + 27: Dallas Kosher (Vaad Hakashrus of Dallas
          + 28: Unknown Kosher Certification
          + 29: United Mehadrin Kosher
          + 30: Orthodox Rabbinical Council of South Florida
          + 31: Ko Kosher Service
          + 32: K'hal Adath Jeshurun
          + 33: Orthodox Rabbicinical Council of British Columbia
          + 34: Kosher Supervisors of Wisconsin
          + 35: Metropolitan Kashruth Council of Michigan
          + 36: Rabbinical Council of New England - Rabbi Abraham Halbfinger - Boston, MA
          + 37: Natural Food Certifiers
          + 38: California K Igud Hakashrus of Los Angeles (Kehillah Kosher)
          + 39: THE HEART "K" (Kehillah Kosher)
          + 40: Certified Kosher Underwriters
          + 41: Central Rabbinical Congress (Hisachdus Harabanim
          + 42: Blue Ribbon Kosher
          + 43: Atlanta Kashrus Commission
          + 44: Debracin (Rabbi Shlomo Stern)
          + 45: Florida K and Florida Kashrus Services
          + 46: Jersey Shore Orthodox Rabbinate (J.S.O.R.)
          + 47: Rabbi Benjamin Kaplinsky
          + 48: Kashrus Council of Lakewood N.J.
          + 49: Council of Orthodox Rabbis of Greater Detroit (K-COR)
          + 50: Young Israel of West Hempstead
          + 51: Kosher Certification Service
          + 52: The Lehigh Valley Kashrus Commission (LVKC)
          + 53: Rabbi Yitzchok M. Leizerowski
          + 54: National Kashrus (NK)
          + 55: Rabbinical Council of Orange County &amp; Long Beach (Orange K)
          + 56: South Palm Beach Vaad (ORB)
          + 57: Org. of Orthodox Kashrus Supervision
          + 58: International Kosher Supervision/Texas "K" Chicago Rabbinical Council
          + 59: Orthodox Vaad of Philadelphia
          + 60: Rabbinical Council of California (RCC)
          + 61: Star-K Kosher Certification (chalav Yisrael) &amp; Star-D Certification (non-chalav Yisrael)
          + 62: Rabbi Nuchem Efraim Teitelbaum (Volover Rav)
          + 63: Vaad Hakashrus of K'hal Machzikei Hadas of Belz
          + 64: Dallas Kosher (Vaad Hakashrus of Dallas)
          + 65: Vaad Hakashrus of Northern California Administrative Offices
          + 66: Vaad Hakashrus of Rochester (VKR)
          + 67: Vaad Harabanim of the Five Towns and Far Rockaway
          + 68: Vaad Harabanim of Flatbush
          + 69: Vaad Harabanim of Greater Seattle
          + 70: Vaad Harabanim of Greater Washington
          + 71: Vaad of Lancaster / Cong. Degel Israel
          + 72: Kedassia, The Joint Kashrus Committee of England|
          + 73: K'hal Machzikei Hadas Edgeware
          + 74: London Beth Din Kashrut Division
          + 75: Machzikei Hadas Manchester
          + 76: Manchester Beis Din
          + 77: Dayan Osher Yaakov Westheim
          + 78: Kashrus Council of Canada (COR)
          + 79: Montreal Vaad Hair
          + 80: The NSW Kashrus Authority|
          + 81: The Beis Din Tzedek of Agudas Israel Moetzes Hakashrus
          + 82: The Beis Din Tzedek of the Eidah Hachareidis of Jerusalem Binyanei Zupnick
          + 83: The Beis Din Tzedek of K'hal Machzikei Hadas - Maareches Hakashrus
          + 84: Chug Chasam Sofer
          + 85: Rabbi Moshe Yehudah Leib Landau
          + 86: Rabanut Hareishit Rechovot
          + 87: Rabanut Yerushalayim Mehadrin
          + 88: Shearis Yisrael
          + 89: S.I.K.S. Ltd./ Services International Kosher Supervision
          + 90: Communidade Ortodoxa Israelita Kehillas Hachareidim Departmento de Kashrus
          + 91: HKK Kosher Certification Service
          + 92: Adas Yereim of Paris Rabbi Y.D. Frankfurter
          + 93: Rabbi Mordechai Rottenberg (Chief Orthodox Rav of Paris)
          + 94: Kashrut Department of Maguen David Community in Mexico City
          + 95: K'hal Chizuk Hadas of Flatbush
          + 96: Beis Din of Crown Heights Vaad Hakashrus
          + 97: Orthodox Rabbinical Council of British Columbia
          + 98: Vaad Hoeir of Saint Louis
          + 99: Rabbi Gershon Mendel Garelik - Italy
          + 100: Chug Chasam Sofer
          + 101: The Bais Din Tzedek of Agudath Israel, Moetzes HaKashrus
          + 102: Kedassia - The Joint Kashrus Committee of England
          + 103: Bais Ben Zion Kosher Certification
          + 104: Rabbi Shmuel Yaffa-Shlessinger (Strasbourg)
          + 105: Kashrus Department of the Beth Din of Johannesburg
          + 106: The Association for Reliable Kashrus
          + 107: Igud Hakashrus of Los Angeles(Kehillah Kosher)
          + 108: Vaad Hakashrus of Northern California
          + 109: Kosher Miami Vaad HaKashrus of Miami-Dade
          + 110: Orthodox Rabbinate of North Dade
          + 111: Indianapolis Beth Din
          + 112: Indianapolis Orthodox Board of Kashrus
          + 113: Iowa "Chai-K" Kosher Supervision
          + 114: Louisville Vaad Hakashrut
          + 115: Louisiana Kashrut Committee
          + 116: New England Kashrus LeMehadrin
          + 117: Vaad Hakashrus of Worcester
          + 118: Double U Kashrus Badatz Mehadrin USA
          + 119: Rabbinical Council of Bergen County
          + 120: Central Rabbinical Congress (Hisachdus HaRabanim)
          + 121: Vaad HaRabonim of Queens
          + 122: Vaad Harabanim of the Five Towns and Far Rockaway
          + 123: Cleveland Kosher
          + 124: Achdus Yisroel
          + 125: Melbourne Kashrut
          + 126: Far East Kashrut
          + 127: Gateshead Kashrus Authority
          + 128: Earth K
          + 129: Kosher Information Bureau
          + 130: Central California Kosher
          + 131: IKS Chicago Rabbinical Council
          + 132: Oregon Kosher
          + 133: Houston Kashruth Association
          + 134: Global Kosher
          + 135: Rabbi Mordechai Kaplinsky
          + 136: Quality Kosher Supervision
          + 137: Vaad Hakashrus of Tidewater, VA
          + 138: Vaad of Cincinnati
          + 139: Community Kashrut of Greater Philadelphia
          + 140: Kosher Los Angeles (KOLA)
          + 141: Va'ad HaKashruth of the Capital District (Albany, NY)
          + 142: Rabbi Aaron Teitelbaum (Nirbater Rav)
          + 143: New Square Kosher Council
          + 144: Rabbi Binyamin Gruber
          + 145: Rabbi M. Weissmandl (Rav of Nitra-Monsey)
          + 146: Kosher Maguen David Mexico
          + 147: Bedatz Mehadrin, Rehovot
          + 148: Kosher Kiwi of New Zealand
          + 149: Vaad Hakashrus of Northern California
          + 150: Midwest Kosher
          + 151: Kosher Organics
          + 152: Star-K Kosher Certification (chalav Yisrael)
          + 153: Coordinated Kosher Supervision
          + 154: Rabbi Aryeh Geretz
          + 155: SKS Kosher Certification Services
          + 156: Vaad Hakehilot of Memphis
          + 157: Federation of Synagogues
          + 158: Ajdut Israel Kosher
          + 159: UK Kashrut
          + 160: PARVE
          + 161: The 'United States K
          + 162: PAREVE
          + 163: Triangle Kosher Certification
          + 164: Vaad Ha'ir of Winnipeg
          + 165: Kosher Check
          + 166: Kosher Supervisores En Alimentos S.C.
          + 167: Cherry K Vaad Hakashruth
          + 168: Harav A. Wosner, Chug Chatam Sofer, Petach Tikvah, Israel
          + 169: Kosher Michigan Certification Agency
          + 170: Kosher Orthodox Certification Service
          + 171: Lancaster County Kosher
          + 172: Organization of Orthodox Kashruth Supervision
          + 173: Ottawa Vaad Hakashrut(OVH)
          + 174: Rabbi Nahum Ephraim Teitelboim, Volover Rav, NY, U.S.A.
          + 175: Rabino Abraham Benhamu, Communidad Judia del Peru
          + 176: Kosher Parve Madrid
          + 999: Generic Kosher code "K"
          
        - container_refunds (Sequence[ContainerRefund]): A list of `ContainerRefund` instances which indicate  
          the deposit amount for this item's container, by state.
        - certifications (Sequence[str]): A list of `certifications <https://
          pim.itemmaster.com/uimSiteContent/common/images/codes/certification>` applicable to this product.
          Valid values include:

          + 130: USDA Organic
          + 131: Non-GMO Project Certified
          + 132: Certified Gluten Free
          + 133: Certified B Corporation
          + 134: Quality Assurance International Certified Organic
          + 135: Certified Vegan
          + 136: California Certified Organic Farmers
          + 137: Fair Trade Certified
          + 138: Rainforest Alliance Certified
          + 139: Oregon Tilth Certified Organic
          + 140: American Heart Association Heart Healthy Certification
          + 141: American Humane Association
          + 142: Animal Welfare Approved
          + 143: Aurora Certified Organic
          + 144: Best Aquaculture Practices Certification
          + 145: Bird Friendly Coffee (Smithsonian Migratory Bird Center)
          + 146: Blue Angel Certification
          + 147: British Retail Consortium
          + 148: CE Marking
          + 149: Certified Angus Beef
          + 150: Certified Humane
          + 151: Cradle-to-Cradle
          + 152: Dolphin Safe
          + 153: US EPA Safer Choice
          + 154: Fish Wise
          + 155: Food Justice
          + 156: Forest Stewardship Council
          + 157: Food Safety System Certification 22000
          + 158: Good Housekeeping
          + 159: Good Housekeeping Green Seal
          + 161: Green Shield Certified
          + 162: Halal(Ifranca)
          + 163: Intertek
          + 164: Leaping Bunny (Coalition for consumer Information on Cosmetics)
          + 165: Marine Stewardship Council
          + 166: NSF Certification
          + 167: REAL® Seal
          + 168: Roundtable on Sustainable Palm Oil
          + 169: Safe Quality Food
          + 170: Salmon Safe
          + 171: Underwriters Laboratory
          + 172: USDA Certified Biobased Product
          + 173: USDA Process Verified Grassfed
          + 174: Whole Grains Council Stamps
          + 175: Nature's Healthiest Certified
          
        - recipes (Sequence[str]): A list of recipes found on the packaging of this product.
        - food_related_indicators (FoodRelatedIndicators): A collection of indicators which are pertinent to food 
          products, including compliance with various dietary restrictions, preparation state, storage temperature,
          etc.
        - nutritions (Sequence[Nutrition]): A sequence of `Nutrition` instances, each representing a nutrition panel
          from the product packaging.
    """

    elements_properties = OrderedDict([
        ('ndcCode', ('ndc_code', str)),
        ('npnCode', ('npn_code', str)),
        ('dinCode', ('din_code', str)),
        ('alcoholByVolumePercent', ('alcohol_by_volume_percent', str)),
        ('alcoholType', ('alcohol_type', str)),
        ('ingredients', ('ingredients', str)),
        ('vitaminsAndMinerals', ('vitamins_and_minerals', str)),
        ('nutritionalClaims', ('nutritional_claims', NutritionalClaims)),
        ('kosherCodes/kosherCode', ('kosher_codes', (str,))),
        ('containerRefunds/containerRefund', ('container_refunds', (ContainerRefund,))),
        ('certifications/certificate', ('certifications', (str,))),
        ('recipes/recipe', ('recipes', (str,))),
        ('foodRelatedIndicators', ('food_related_indicators', FoodRelatedIndicators)),
        ('nutritions/nutrition', ('nutritions', (Nutrition,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='grocery'  # type: str
    ):
        self.ndc_code = None  # type: Optional[str]
        self.npn_code = None  # type: Optional[str]
        self.din_code = None  # type: Optional[str]
        self.alcohol_by_volume_percent = None  # type: Optional[Decimal]
        self.alcohol_type = None  # type: Optional[str]
        self.ingredients = None  # type: Optional[str]
        self.vitamins_and_minerals = None  # type: Optional[str]
        self.nutritional_claims = None  # type: Optional[NutritionalClaims]
        self.kosher_codes = []  # type: Sequence[str]
        self.container_refunds = []  # type: Sequence[ContainerRefund]
        self.certifications = []  # type: Sequence[str]
        self.recipes = []  # type: Sequence[str]
        self.food_related_indicators = None  # type: Optional[FoodRelatedIndicators]
        self.nutritions = []  # type: Sequence[Nutrition]
        super().__init__(xml=xml, tag=tag)


class Product(SOAPElement):
    """
    Properties:
    
        - sequence (int): The display sequence for product information. When multiple instances of `Product` are 
          associated with an item, it is typically because the item is a variety pack (sold as a single consumer unit), 
          and different information applicable to items within the variety pack. The first instance of `Product`,
          where ``sequence = 1``, typically applies to the product as a whole (and will, as a result, often exclude
          nutrition information and other more specific details). 
        - id (str): A unique identifier for this product.
        - type (str): A high-level product classification, such as "grocery".
        - size (Measurement)
        - description (str): A one-line description of the product, usually beginning with the brand name, for example: 
          "Parks Famous Flavor Hot Smoked Turkey Sausage" or "Hatfield Recipe Essentials Ground Sausage Sweet Italian".
        - distributor (ValueWithID): Identifies the company responsible for distributing this product.
        - importer (ValueWithID): Identifies the company responsible for importing this product.
        - brand (ValueWithID)
        - sub_brands (Sequence[str]): A secondary level of branding attributed to the product, often a trademarked name
          for a product line. In "Coca-Cola Classic", "Coca-Cola" is the brand and "Classic" is the sub-brand.
        - brand_description (str): This is usually the same as ``brand.text``, but not tied to a brand ID, and therefore
          can vary by item, and will occasionally include sub-branding such as "Coca-Cola Classic" rather than simply
          "Coca-Cola".
        - secondary_brand_description (str)
        - manufacturer (ValueWithID): The name and a unique identifier of the company who manufactured this product.
        - line (str): The name of the the product line to which this belongs. A *product line* is a group of related 
          products, under a single brand, sold by the same company.
        - variety (str): A differentiation between variations of a *product line* such as a flavor, color, scent, etc.
        - seasonal (bool): If *True*, this indicates that the item is only available for a limited period of 
          distribution each year.
        - country_of_origin (str): A comma-separated list of the country or countries where this product was grown, 
          manufactured, or assembled.
        - warnings (str): Warnings and precautions (usually those which are found on a product's packaging).
        - drug_interactions (str): Information labeled "interactions" on over-the-counter or prescription medications.
        - directions (str): Usage instructions for this product, as found on the product's packaging.
        - indications (str): Indicates what ailments a product (typically an over-the-counter medication) can/should 
          be used to treat.
        - grocery (Grocery): A collection of attributes exclusively pertinent to grocery items.
    """

    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('@type', ('type', str)),
        ('@sequence', ('sequence', int)),
        ('description', ('description', str)),
        ('size', ('size', Measurement)),
        ('description', ('description', str)),
        ('distributor', ('distributor', ValueWithID)),
        ('importer', ('importer', ValueWithID)),
        ('brand', ('brand', ValueWithID)),
        ('subBrands/subBrand', ('sub_brands', (str,))),
        ('brandDescription', ('brand_description', str)),
        ('secondaryBrandDescription', ('secondary_brand_description', str)),
        ('manufacturer', ('manufacturer', ValueWithID)),
        ('line', ('line', str)),
        ('variety', ('variety', str)),
        ('seasonal', ('seasonal', bool)),
        ('countryOfOrigin', ('country_of_origin', str)),
        ('warnings/warning', ('warnings', (str,))),
        ('drugInteractions', ('drug_interactions', str)),
        ('directions', ('directions', str)),
        ('indications', ('indications', str)),
        ('grocery', ('grocery', Grocery))
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='product'  # type: str
    ):
        # type: (...) -> None
        self.sequence = None  # type: Optional[int]
        self.id = None  # type: Optional[str]
        self.type = None  # type: Optional[str]
        self.size = None  # type: Optional[Measurement]
        self.description = None  # type: Optional[str]
        self.distributor = None  # type: Optional[ValueWithID]
        self.importer = None  # type: Optional[ValueWithID]
        self.brand = None  # type: Optional[ValueWithID]
        self.sub_brands = []  # type: Sequence[str]
        self.brand_description = None  # type: Optional[str]
        self.secondary_brand_description = None  # type: Optional[str]
        self.manufacturer = None  # type: Optional[ValueWithID]
        self.line = None  # type: Optional[str]
        self.variety = None  # type: Optional[str]
        self.seasonal = None  # type: Optional[bool]
        self.country_of_origin = None  # type: Optional[str]
        self.warnings = []  # type: Sequence[str]
        self.drug_interactions = None  # type: Optional[str]
        self.directions = None  # type: Optional[str]
        self.indications = None  # type: Optional[str]
        self.grocery = None  # type: Optional[Grocery]
        super().__init__(xml=xml, tag=tag)


class Company(SOAPElement):
    """
    Properties:

        - name (str)
        - address (str)
        - address2 (str)
        - city (str)
        - state (str)
        - zip (str)
        - country (str)
        - email (str)
        - phone (str)
        - url (str)
        - fax (str)
    """

    elements_properties = OrderedDict([
        ('name', ('name', str)),
        ('address', ('address', str)),
        ('address2', ('address2', str)),
        ('city', ('city', str)),
        ('state', ('state', str)),
        ('zip', ('zip', str)),
        ('country', ('country', str)),
        ('email', ('email', str)),
        ('phone', ('phone', str)),
        ('url', ('url', str)),
        ('fax', ('fax', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='company'  # type: str
    ):
        self.name = None  # type: Optional[str]
        self.address = None  # type: Optional[str]
        self.address2 = None  # type: Optional[str]
        self.city = None  # type: Optional[str]
        self.state = None  # type: Optional[str]
        self.zip_code = None  # type: Optional[str]
        self.country = None  # type: Optional[str]
        self.email = None  # type: Optional[str]
        self.phone = None  # type: Optional[str]
        self.url = None  # type: Optional[str]
        self.fax = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Attribute(SOAPElement):

    """
    An arbitrary attribute/value pair.
    
    Properties:

        - name (str)
        - value (str)
    """

    elements_properties = OrderedDict([
        ('name', ('name', str)),
        ('value', ('value', str))
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='attribute'  # type: str
    ):
        # type: (...) -> None
        name = None  # type: Optional[str]
        value = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Item(SOAPElement):

    """
    Properties:

        - id (str): A unique identifier for this item.
        - type (str): A high-level classification for this item, such as "grocery".
        - status (str): "P" indicates "published".
        - item_master_name (str): A one-line description of the item, typically identical to the
          *name*, as well as to the product description associated with the product with a
          sequence value of *1*.
        - sell_copy (str): A brief paragraph containing marketing copy.
        - bullet_points (Sequence[str]): A sequence of features, usually intended to accompany 
          the `sell_copy`.
        - ecommerce_description (str): A one-line description intended for ecommerce use.
        - manufacturer_ecommerce_description (str): A one-line description intended for
          ecommerce use, provided by the manufacturer.
        - name (str): A one-line description of the item.
        - marketing_description (str): An often incoherent assemblage of text captured from the
          product's packaging—not typically usable for marketing purposes. Refer to `sell_copy`
          for more coherent copy.
        - other_description (str): Additional text captured from the product's packaging.
        - upcs (Sequence[UPC]): A sequence of one or more universal product codes used to
          identify this item.
        - manufacturer (ValueWithID): The name and unique ID of this product's manufacturer.
        - item_retailer (ValueWithID): The name and unique ID of this product's retailer.
        - categories (Sequence[Category]): A list of `GPC bricks <http://www.gs1.org/gpc>`
          associated with this item.
        - private_label_item (bool): Is this a private label product?
        - created (XFormSource): When was this product created?
        - last_updated (XFormSource): When was this product last updated?
        - distributor (ValueWithID): The company responsible for distributing this product.
        - importer (ValueWithID): The company responsible for importing this product.
        - manufacturer_sku (str): The manufacturer's stock keeping unit for this item.
        - media (Sequence[Medium]): Media (most commonly images) associated with the item.
        - manufacturer_supplied_content_images (Sequence[ManufacturerSuppliedContent]):
          Images, provided by the manufacturer, for this item.
        - manufacturer_supplied_content_logos (Sequence[ManufacturerSuppliedContent]):
          Logos, provided by the manufacturer, for this item.
        - manufacturer_supplied_content_videos (Sequence[ManufacturerSuppliedContent]):
          Videos, provided by the manufacturer, for this item.
        - manufacturer_supplied_content_files (Sequence[ManufacturerSuppliedContent]):
          Media files, provided by the manufacturer, for this item.
        - manufacturer_supplied_content_mobiles (Sequence[ManufacturerSuppliedContent]):
          Mobile optimized images, provided by the manufacturer, for this item.
        - company_supplied_images (Sequence[ManufacturerSuppliedContent]):
          Images, provided by the manufacturer, for this item.
        - package_data (PackageData): Information about the package, such as dimensions, package type, etc.
        - planogram_data (PlanogramData): Planogram and shelf management information.
        - products (Sequence[Product]): A sequence of `Product` instances, each containing information about a product 
          contained. When multiple instances of `Product` are associated with an item, it is typically because the 
          item is a variety pack (sold as a single consumer unit), and different information applicable to items within  
          the variety pack. The first instance of `Product`, where the sequence property is *1*, typically applies to 
          the product as a whole (and will, as a result, often exclude nutrition information and other more specific 
          details).
        - package_distributor (Company): Information about the company responsible for
          distributing this item.
        - package_importer (Company): Information about the company responsible for
          importing this item.
        - package_manufacturer (Company): Information about the company responsible for
          manufacturing this item.
        - attributes (Sequence[Attribute]): A sequence of arbitrary attribute/value pairs.
        - company_tracking_id (CompanyTrackingID)
    """

    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('@type', ('type', str)),
        ('@status', ('status', str)),
        ('companyTrackingId', ('company_tracking_id', str)),
        ('ecommerceDescription', ('ecommerce_description', str)),
        ('manufacturerEcommerceDescription', ('manufacturer_ecommerce_description', str)),
        ('itemMasterName', ('item_master_name', str)),
        ('name', ('name', str)),
        ('marketingDescription', ('marketing_description', str)),
        ('otherDescription', ('other_description', str)),
        ('upcs/upc', ('upcs', (UPC,))),
        ('manufacturer', ('manufacturer', ValueWithID)),
        ('itemRetailer', ('item_retailer', ValueWithID)),
        ('categories/category', ('categories', (Category,))),
        ('privateLabelItem', ('private_label_item', bool)),
        ('created', ('created', XFormSource)),
        ('lastUpdated', ('last_updated', XFormSource)),
        ('distributor', ('distributor', ValueWithID)),
        ('importer', ('importer', ValueWithID)),
        ('manufacturerSku', ('manufacturer_sku', str)),
        ('media/medium', ('media', (Medium,))),
        (
            'manufacturerSuppliedContentImages/medium',
            ('manufacturer_supplied_content_images', (ManufacturerSuppliedContent,))
        ),
        (
            'manufacturerSuppliedContentLogos/medium',
            ('manufacturer_supplied_content_logos', (ManufacturerSuppliedContent,))
        ),
        (
            'manufacturerSuppliedContentVideos/medium',
            ('manufacturer_supplied_content_videos', (ManufacturerSuppliedContent,))
        ),
        (
            'manufacturerSuppliedContentFiles/medium',
            ('manufacturer_supplied_content_files', (ManufacturerSuppliedContent,))
        ),
        (
            'manufacturerSuppliedContentMobiles/medium',
            ('manufacturer_supplied_content_mobiles', (ManufacturerSuppliedContent,))
        ),
        ('companySuppliedImages/medium', ('company_supplied_images', (ManufacturerSuppliedContent,))),
        ('packageData', ('package_data', PackageData)),
        ('planogramData', ('planogram_data', PlanogramData)),
        ('products/product', ('products', (Product,))),
        ('packageDistributor', ('package_distributor', Company)),
        ('packageImporter', ('package_importer', Company)),
        ('packageManufacturer', ('package_manufacturer', Company)),
        ('attributes/attribute', ('attributes', (Attribute,))),
        ('sellCopy', ('sell_copy', str)),
        ('bulletPoints/bulletPoint', ('bullet_points', (str,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='item'  # type: str
    ):
        self.id = None  # type: Optional[str]
        self.type = None  # type: Optional[str]
        self.status = None  # type: Optional[str]
        self.company_tracking_id = None # type: Optional[CompanyTrackingID]
        self.item_master_name = None  # type: Optional[str]
        self.name = None  # type: Optional[str]
        self.marketing_description = None  # type: Optional[str]
        self.other_description = None  # type: Optional[str]
        self.upcs = []  # type: Sequence[UPC]
        self.manufacturer = None  # type: List[ValueWithID]
        self.item_retailer = None  # type: Optional[ValueWithID]
        self.categories = []  # type: Sequence[Category]
        self.private_label_item = None  # type: Optional[bool]
        self.created = None  # type: Optional[XFormSource]
        self.last_updated = None  # type: Optional[XFormSource]
        self.distributor = None  # type: Optional[ValueWithID]
        self.importer = None  # type: Optional[ValueWithID]
        self.manufacturer_sku = None  # type: Optional[str]
        self.media = []  # type: Sequence[Medium]
        self.manufacturer_supplied_content_images = []  # type: Sequence[ManufacturerSuppliedContent]
        self.manufacturer_supplied_content_logos = []  # type: Sequence[ManufacturerSuppliedContent]
        self.manufacturer_supplied_content_videos = []  # type: Sequence[ManufacturerSuppliedContent]
        self.manufacturer_supplied_content_files = []  # type: Sequence[ManufacturerSuppliedContent]
        self.manufacturer_supplied_content_mobiles = []  # type: Sequence[ManufacturerSuppliedContent]
        self.company_supplied_images = []  # type: Sequence[ManufacturerSuppliedContent]
        self.package_data = None  # type: Optional[PackageData]
        self.planogram_data = None  # type: Optional[PlanogramData]
        self.products = []  # type: Sequence[Product]
        self.package_distributor = None  # type: Optional[Company]
        self.package_importer = None  # type: Optional[Company]
        self.package_manufacturer = None  # type: Optional[Company]
        self.attributes = []  # type: Sequence[Attribute]
        self.sell_copy = None  # type: Optional[str]
        self.bullet_points = []  # type: Sequence[str]
        self.ecommerce_description = None  # type: Optional[str]
        self.manufacturer_ecommerce_description = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Manufacturer(SOAPElement):
    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('name', ('name', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='manufacturer',  # type: str
    ):
        self.id = None  # type: Optional[str]
        self.name = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Brand(SOAPElement):
    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('name', ('name', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='brand',  # type: str
    ):
        self.id = None  # type: Optional[str]
        self.name = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Retailer(SOAPElement):
    elements_properties = OrderedDict([
        ('@id', ('id', str)),
        ('name', ('name', str)),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='retailer',  # type: str
    ):
        self.id = None  # type: Optional[str]
        self.name = None  # type: Optional[str]
        super().__init__(xml=xml, tag=tag)


class Items(SOAPElement):
    """    
    Properties:

        - start (int)
        - count (int)
        - total (int)
        - item (Sequence[Item])
    """

    elements_properties = OrderedDict([
        ('@start', ('start', int)),
        ('@count', ('count', int)),
        ('@total', ('total', int)),
        ('item', ('item', (Item,)))
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='item'  # type: str
    ):
        self.start = None  # type: Optional[int]
        self.count = None  # type: Optional[int]
        self.total = None  # type: Optional[int]
        self.item = []  # type: Sequence[Item]
        super().__init__(xml=xml, tag=tag)


class Manufacturers(SOAPElement):
    """
    Properties:

        - number_of_results (int)
        - results_per_page (int)
        - start_index (int)
        - manufacturers (Sequence[Manufacturer])
    """

    elements_properties = OrderedDict([
        ('numberOfResults', ('number_of_results', int)),
        ('resultsPerPage', ('results_per_page', int)),
        ('startIndex', ('start_index', int)),
        ('manufacturer', ('manufacturer', (Manufacturer,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='manufacturers',  # type: str
    ):
        self.number_of_results = None  # type: Optional[int]
        self.results_per_page = None  # type: Optional[int]
        self.start_index = None  # type: Optional[int]
        self.manufacturer = []  # type: Sequence[Manufacturer]
        super().__init__(xml=xml, tag=tag)


class Brands(SOAPElement):
    """
    Properties:

        - number_of_results (int)
        - results_per_page (int)
        - start_index (int)
        - brands (Sequence[Brand])
    """

    elements_properties = OrderedDict([
        ('numberOfResults', ('number_of_results', int)),
        ('resultsPerPage', ('results_per_page', int)),
        ('startIndex', ('start_index', int)),
        ('brand', ('brand', (Brand,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='brands',  # type: Sequence[Manufacturer]
    ):
        self.number_of_results = None  # type: Optional[int]
        self.results_per_page = None  # type: Optional[int]
        self.start_index = None  # type: Optional[int]
        self.brand = []  # type: Sequence[Brand]
        super().__init__(xml=xml, tag=tag)


class Retailers(SOAPElement):
    """
    Properties:

        - number_of_results (int)
        - results_per_page (int)
        - start_index (int)
        - retailer (Sequence[Retailer])
    """

    elements_properties = OrderedDict([
        ('numberOfResults', ('number_of_results', int)),
        ('resultsPerPage', ('results_per_page', int)),
        ('startIndex', ('start_index', int)),
        ('retailer', ('retailer', (Retailer,))),
    ])

    def __init__(
        self,
        xml=None,  # type: Optional[Union[Element, str, HTTPResponse]]
        tag='brands',  # type: str
    ):
        self.number_of_results = None  # type: Optional[int]
        self.results_per_page = None  # type: Optional[int]
        self.start_index = None  # type: Optional[int]
        self.retailer = []  # type: Sequence[Retailer]
        super().__init__(xml=xml, tag=tag)